## -------------------------------------------------------
##
## File: rivermanagement_Write.jl
##
## August 14, 2022 -- Peter Reichert
## peter.reichert@emeriti.eawag.ch
##
## -------------------------------------------------------

using  DataFrames
using  Statistics
import CSV

function rivermanagement_Write(net,
                               file_reach,
                               file_node;
                               sep = '\t')

    # check arguments:
    # ================

    if ! ( haskey(net,"attrib_reach") & haskey(net,"attrib_node") )
        println("rivermanagement_Write: *** argument \"net\" needs keys \"attrib_reach\" and \"attrib_node\" ***")
        return
    end

    # write reach and node attributes:
    # ================================

    CSV.write(file_reach,net["attrib_reach"];delim=sep)
    CSV.write(file_node ,net["attrib_node"] ;delim=sep)

    println(string("rivermanagement_Write: reach and node attributes written to\n",
                   "                  \"",file_reach,"\"\n",
                   "                and\n",
                   "                  \"",file_node,"\""))

    return

end

