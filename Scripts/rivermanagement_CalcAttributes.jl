## -------------------------------------------------------
##
## File: rivermanagement_CalcAttributes.jl
##
## August 15, 2023 -- Peter Reichert
## peter.reichert@emeriti.eawag.ch
##
## -------------------------------------------------------


function rivermanagement_CalcAttributes(net::Dict,                   # rivernet
                                        barrier_nat::Vector{Bool},   # which nodes are natual barriers?
                                        barrier_art::Vector{Bool},   # which nodes are artificial barriers?
                                        badreach_nat::Vector{Bool},  # which reaches are inappropriate naturally? (e.g. too steep for fish)
                                        badreach_art::Vector{Bool};  # which reaches are inappropriate artificially? (e.g. culverts)
                                        thresh_length = 25.0,        # threshold length for inappropriate reaches
                                        col_length    = "length",    # column name in net["attrib_reach"] that contains the reach length
                                        col_order     = "order",     # column name in net["attrib_reach"] that contains the stream order
                                        reach_morph   = 1.0,         # value or array with reach morphological values
                                        reach_imp     = 1.0,         # value or array with reach fish ecological importance values
                                        verbose       = true)
    # function to aggregate attributes by region:

    function aggregate_by_region(what,by,fun,name_what)

        # aggregate "what":

        res = combine(groupby(DataFrame(x=what,y=by),:y),:x.=>fun)
        sort!(res,1)
        rename!(res,["region",name_what])

        return res
    
    end

    function aggregate_attributes(connected_regions_nat,
                                  connected_regions_inv,
                                  assoc_regions_nat,
                                  reach_length,
                                  reach_order,
                                  reach_morph,
                                  reach_imp)

        # get region attribute sums under natural conditions and considering
        # artificial barriers

        region_lengthordermorphimp_inv = 
            aggregate_by_region(reach_length .* reach_order .* reach_morph .* reach_imp,
                                connected_regions_inv,sum,"att");
        region_lengthorderimp_inv =
            aggregate_by_region(reach_length .* reach_order .* reach_imp,
                                connected_regions_inv,sum,"att");
        region_lengthorderimp_nat =
            aggregate_by_region(reach_length .* reach_order .* reach_imp,
                                connected_regions_nat,sum,"att");
 
        # calculate offsets:

        offset_inv = 0; if region_lengthordermorphimp_inv[1,1] == 0; offset_inv = 1; end
        offset_nat = 0; if region_lengthorderimp_nat[1,1] == 0; offset_nat = 1; end
        
        # calculate upstream attributes:

        h_ups_inv = region_lengthordermorphimp_inv[offset_inv+1,2]
        h_ups_nat = region_lengthorderimp_nat[offset_nat+1,2]
        a_ups     = h_ups_inv/h_ups_nat

        # calculate internal attributes:

        h_int_inv = 0.0
        for i in 1:(nrow(region_lengthordermorphimp_inv)-offset_inv)
            h_int_inv = h_int_inv + 
                            region_lengthorderimp_inv[offset_inv+i,2] * 
                            region_lengthordermorphimp_inv[offset_inv+i,2] / 
                            region_lengthorderimp_nat[offset_nat+assoc_regions_nat[i],2]
        end
        h_int_nat = sum(region_lengthorderimp_nat[(offset_nat+1):nrow(region_lengthorderimp_nat),2])
        a_int = h_int_inv/h_int_nat

        # return attributes:

        return Dict("h_ups_inv" => h_ups_inv,
                    "h_ups_nat" => h_ups_nat,
                    "a_ups"     => a_ups,
                    "h_int_inv" => h_int_inv,
                    "h_int_nat" => h_int_nat,
                    "a_int"     => a_int)
    
    end

    # initialization:

    n_reach = nrow(net["attrib_reach"])
    if ! isa(reach_morph,Array) 
        reach_morph_local = fill(reach_morph,n_reach)
    elseif length(reach_morph) == 1
        reach_morph_local = fill(reach_morph[1],n_reach)
    elseif length(reach_morph) == n_reach
        reach_morph_local = copy(reach_morph)
    else
        println(string("calc_attributes: *** argument \"reach_morph\" ",
                      "has to be a float scalar or vector of length 1 or number of reaches"))
    end
    reach_morph_local[badreach_art] .= 0.0
    if ! isa(reach_imp,Array) 
        reach_imp_local = fill(reach_imp,n_reach)
    elseif length(reach_morph) == 1
        reach_imp_local = fill(reach_imp[1],n_reach)
    elseif length(reach_imp) == n_reach
        reach_imp_local = copy(reach_imp)
    else
        println(string("calc_attributes: *** argument \"reach_morph\" ",
                      "has to be a float scalar or vector of length 1 or number of reaches"))
    end
    reach_length_local = net["attrib_reach"][:,col_length]
    reach_order_local  = net["attrib_reach"][:,col_order]
    replace!(reach_order_local,missing=>1)        # missing order here 1
    replace!(reach_order_local,0=>1)              # on some files, missing is coded as 0

    # get connected regions under natural conditions and considering 
    # artificial barriers:
    
    connected_regions_nat = rivermanagement_ConnectedRegions(
                                net;
                                crit_reach    = .! badreach_nat,
                                crit_node     = .! barrier_nat,
                                thresh_length = thresh_length,
                                verbose       = verbose)  
    connected_regions_inv = rivermanagement_ConnectedRegions(
                                net;
                                crit_reach    = (.! badreach_nat) .& (.! badreach_art),
                                crit_node     = (.! barrier_nat)  .& (.! barrier_art),
                                thresh_length = thresh_length,
                                verbose       = verbose)

    # get indices of corresponding natural connected regions for each region
    # when considering artificial barriers:

    assoc_regions_nat = Vector{Union{Missing,Int}}(missing,maximum(connected_regions_inv))
    for i in 1:length(assoc_regions_nat)
        ind = findall(isequal(i),connected_regions_inv)
        if length(ind) == 0
            error(string("*** inconsistent natural and investigated regions in function calc_attributes; inv region ",i," not found"))
        end
        assoc_regions_nat[i] = connected_regions_nat[ind[1]]
        natregs = unique(connected_regions_nat[ind])
        if length(natregs) > 1
            println(string("*** inconsistent natural and investigated regions in function calc_attributes; inv region ",i,
                           " correspnonds to multiple regions nat: ",natregs))
        end
    end

    reach_ones = ones(Float64,n_reach)
    att                = aggregate_attributes(connected_regions_nat,
                                              connected_regions_inv,
                                              assoc_regions_nat,
                                              reach_length_local,
                                              reach_order_local,
                                              reach_morph_local,
                                              reach_imp_local)
    att_goodmorph      = aggregate_attributes(connected_regions_nat,
                                              connected_regions_inv,
                                              assoc_regions_nat,
                                              reach_length_local,
                                              reach_order_local,
                                              reach_ones,
                                              reach_imp_local)
    att_noimp          = aggregate_attributes(connected_regions_nat,
                                              connected_regions_inv,
                                              assoc_regions_nat,
                                              reach_length_local,
                                              reach_order_local,
                                              reach_morph_local,
                                              reach_ones)
    att_goodmorphnoimp = aggregate_attributes(connected_regions_nat,
                                              connected_regions_inv,
                                              assoc_regions_nat,
                                              reach_length_local,
                                              reach_order_local,
                                              reach_ones,
                                              reach_ones)

    # return attributes:

    return Dict("regions_nat"        => connected_regions_nat,
                "regions_inv"        => connected_regions_inv,
                "assoc_regions_nat"  => assoc_regions_nat,
                "n_nat"              => maximum(connected_regions_nat),
                "n_inv"              => maximum(connected_regions_inv),
                "att"                => att,
                "att_goodmorph"      => att_goodmorph,
                "att_noimp"          => att_noimp,
                "att_goodmorphnoimp" => att_goodmorphnoimp)

end

