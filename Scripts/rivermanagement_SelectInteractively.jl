## -------------------------------------------------------
##
## File: rivermanagement_SelectInteractively.jl
##
## June 08, 2023 -- Peter Reichert
## peter.reichert@emeriti.eawag.ch
##
## -------------------------------------------------------


function rivermanagement_RemoveBarriersCulverts(
            net::Dict,                              # rivernet
            barrier_nat::Vector{Vector{Bool}},      # which nodes are natual barriers? (vector across different criteria, typically heights)
            barrier_art::Vector{Vector{Bool}},      # which nodes are artificial barriers? (vector across different criteria, typically heights)
            toosteep::Vector{Vector{Bool}},
            culvert::Vector{Bool};                  # which reaches are culverts?
            crit_name        = [],                  # names of criteria for barriers (same dimension as barrier_... required)
            thresh_length    = 50.0,                # critical length of culverts to count as barrier
            reach_morph      = 1.0,                 # morphological state of reaches in [0.0,1.0]
            reach_imp        = 1.0,                 # ecological potential, default 1, higher values for higher potential
            col_length       = "length",
            col_width        = "width",
            col_order        = "order",
            plot_id          = "1",
            size_x           = 1000,
            title            = "",
            dir_out          = ".",
            plot_reach_ids   = false,
            barriers_removed = Int[],
            culverts_removed = Int[])
       
    # check input and initialize variables:
    # -------------------------------------

    lwd_reach   = 3.0
    lwd_culvert = 6.0
    lwd_missing = 1.0

    n_reach     = nrow(net["attrib_reach"])
    n_node      = nrow(net["attrib_node"])    
    n_crit      = length(barrier_nat)

    barrier_labels = crit_name; if length(crit_name)!=n_crit; barrier_labels=string.("Reg ",1:n_crit); end

    if ( length(barrier_art) != n_crit ); error("*** rivermanagement_RemoveBarriersCulverts: inconsistent number of criteria vs. barrier definitions"); end
    if ( length(toosteep)    != n_crit ); error("*** rivermanagement_RemoveBarriersCulverts: inconsistent number of criteria vs. too steep reaches"); end

    culvert_local = fill(false,n_reach)
    if ! isa(culvert,Array)
        culvert_local = fill(culvert,n_reach)
    elseif length(culvert) == 1
        culvert_local = culvert[1]
    elseif length(culvert) == n_reach
        culvert_local = copy(culvert)
    elseif length(culvert) != 0
        println(string("rivermanagement_RemoveBarriersCulverts: *** \"culvert\" needs to be a logical scalar ",
                       "or a vector of length 0, 1 or number of reaches ***"))
    end
 
    reach_morph_local = fill(1.0,n_reach)
    if ! isa(reach_morph,Array)
        reach_morph_local = fill(reach_morph,n_reach)
    elseif length(reach_morph) == 1
        reach_morph_local = reach_morph[1]
    elseif length(reach_morph) == n_reach
        reach_morph_local = copy(reach_morph)
    elseif length(reach_morph) != 0
        println(string("rivermanagement_RemoveBarriersCulverts: *** \"reach_morph\" needs to be a real scalar ",
                       "or a vector of length 0, 1 or number of reaches ***"))
    end
    reach_morph_local_missing1 = copy(reach_morph_local)
    replace!(reach_morph_local_missing1,missing=>1.0)
    
    reach_imp_local = fill(1.0,n_reach)
    if ! isa(reach_imp,Array)
        reach_imp_local = fill(reach_imp,n_reach)
    elseif length(reach_imp) == 1
        reach_imp_local = fill(reach_imp[1],n_reach)
    elseif length(reach_imp) == n_reach
        reach_imp_local = copy(reach_imp)
    elseif length(reach_imp) != 0
        println(string("rivermanagement_RemoveBarriersCulverts: *** \"reach_imp\" needs to be a real scalar ",
                       "or a vector of length 0, 1 or number of reaches ***"))
    end
    replace!(reach_imp_local,missing=>1.0)

    attrib = Array{Any}(undef,n_crit)
    for j in 1:n_crit
    
        attrib[j] = rivermanagement_CalcAttributes(net,
                                                   barrier_nat[j],
                                                   barrier_art[j],
                                                   toosteep[j],
                                                   culvert_local,
                                                   thresh_length = thresh_length,
                                                   col_length    = col_length,
                                                   col_order     = col_order,
                                                   reach_morph   = reach_morph_local_missing1,
                                                   reach_imp     = reach_imp_local)

    end

    subid = 0

    barrier_art_local      = deepcopy(barrier_art)                # keep original for later events (array of different barrier type)
    barriers_removed_local = copy(sort(unique(barriers_removed))) # keep original for later events
    culvert_local          = copy(culvert)                        # keep original for later events
    culverts_removed_local = copy(sort(unique(culverts_removed))) # keep original for later events

    col_morph = rivermanagement_GetMSKColor.(reach_morph_local)
    col_morph[findall(culvert_local)] .= "black"
    col_morph[findall(ismissing.(reach_morph_local))] .= "black"
    lwd_morph = fill(lwd_reach,n_reach)
    lwd_morph[findall(culvert_local)] .= lwd_culvert
    lwd_morph[findall(ismissing.(reach_morph_local))] .= lwd_missing

    col_pot = fill("blue",n_reach)
    col_pot[findall(culvert_local)] .= "black"
    col_pot[findall(reach_imp_local.>1)] .= "cyan"
    
    col_reg = Array{Any,1}(undef,n_crit)
    lwd_reg = Array{Any,1}(undef,n_crit)
    for j in 1:n_crit
        col = rivermanagement_GetRegionColors(maximum(attrib[j]["regions_inv"])+1)
        col_reg[j] = col[attrib[j]["regions_inv"].+1]
        lwd_reg[j] = fill(lwd_reach,n_reach)
        lwd_reg[j][findall(culvert_local)] .= lwd_culvert
        lwd_reg[j][findall(toosteep[j])] .= lwd_missing
    end

    size_nodes = ones(Float64,n_crit)
    for j in 1:n_crit; size_nodes[j] = 3+2*j; end

    # define layout and array for traces:
    # -----------------------------------

    len_culvertsremoved = sum(net["attrib_reach"][culverts_removed_local,col_length])
    t = string(title," ",plot_id)
    if ( length(barriers_removed_local)>0 ) | ( length(culverts_removed_local)>0 )
        t = string(t," (",length(barriers_removed_local)," barriers, ",
                   round(len_culvertsremoved,digits=1)," m of culverts removed)")
    end
    x_lim = net["x_lim"]
    y_lim = net["y_lim"] 
    if x_lim[2] == x_lim[1]
        if y_lim[2] == y_lim[1]
            x_lim = [x_lim[1]-1.0,x_lim[2]+1.0] 
            y_lim = [y_lim[1]-1.0,y_lim[2]+1.0] 
        else
            x_lim = [x_lim[1]-0.1*(y_lim[2]-y_lim[1]),x_lim[2]+0.1*(y_lim[2]-y_lim[1])]
        end
    elseif y_lim[2] == y_lim[1]
        y_lim = [y_lim[1]-0.1*(x_lim[2]-x_lim[1]),y_lim[2]+0.1*(x_lim[2]-x_lim[1])]
    end
    layout = PlotlyJS.Layout(title      = t,
                             showlegend = false,
                             autosize   = false,
                             width      = size_x,          # seems not to work
                             height     = round(Int,size_x*(y_lim[2]-y_lim[1])/(x_lim[2]-x_lim[1])))

    traces = PlotlyJS.PlotlyBase.AbstractTrace[]

    # define reach traces:
    # --------------------

    for i in 1:n_reach

        local trace = PlotlyJS.scatter(x          = net["attrib_reach"][i,"coord"].x,
                                       y          = net["attrib_reach"][i,"coord"].y,
                                       mode       = "lines",
                                       line       = PlotlyJS.attr(color=col_reg[1][i],width=lwd_reg[1][i]),
                                       showlegend = false)

        traces = vcat(traces,trace)

    end

    # define node traces:
    # -------------------

    ind_art          = Array{Array{Int,1},1}(undef,n_crit)
    curvenum_barrier = zeros(Int,n_crit)
    for j in 1:n_crit

        ind_nat = findall(barrier_nat[j])
        if length(ind_nat) > 0
            trace = PlotlyJS.scatter(x          = net["attrib_node"][ind_nat,"x"],
                                     y          = net["attrib_node"][ind_nat,"y"],
                                     mode       = "markers",
                                     marker     = PlotlyJS.attr(color  = "blue",
                                                                symbol = "circle",
                                                                line   = PlotlyJS.attr(width=0), # no border
                                                                size   = size_nodes[j]),
                                     showlegend = false)
            traces = vcat(traces,trace)
        end

        ind_art[j]  = findall(barrier_art[j])
        if length(ind_art[j]) > 0
            curvenum_barrier[j] = length(traces)
            trace = PlotlyJS.scatter(x          = net["attrib_node"][ind_art[j],"x"],
                                     y          = net["attrib_node"][ind_art[j],"y"],
                                     mode       = "markers",
                                     marker     = PlotlyJS.attr(color  = "red",
                                                                symbol = "circle",
                                                                line   = PlotlyJS.attr(width=0), # no border
                                                                size   = size_nodes[j]),
                                     showlegend = false)
            traces = vcat(traces,trace)
        else
            curvenum_barrier[j] = 0
        end

    end

    # add culvert traces as scatter trace to allow for selection:
    # -----------------------------------------------------------

    ind_culvert = findall(culvert_local)
    n_culverts = length(ind_culvert)
    if n_culverts > 0
        curvenum_culvert = zeros(Int,n_culverts)
        for j in 1:n_culverts
            curvenum_culvert[j] = length(traces)
            trace = PlotlyJS.scatter(x          = net["attrib_reach"][ind_culvert[j],"coord"].x,
                                     y          = net["attrib_reach"][ind_culvert[j],"coord"].y,
                                     mode       = "markers",
                                     marker     = PlotlyJS.attr(color  = "black",
                                                                symbol = "circle",
                                                                line   = PlotlyJS.attr(width=0), # no border
                                                                size   = 1),
                                     showlegend = false)
            traces = vcat(traces,trace)
        end
    end

    # plot river network:
    # -------------------

    p = PlotlyJS.plot(traces,layout)

    # add buttons:
    # ------------

    buttons = []
    for j in 1:n_crit
        buttons = vcat(buttons,
                       PlotlyJS.attr(args   = [PlotlyJS.attr(line=[PlotlyJS.attr(color=col_reg[j][i],width=lwd_reg[j][i]) for i in 1:n_reach])],
                                     label  = barrier_labels[j],
                                     method = "restyle"))
    end
    buttons = vcat(buttons,
                   PlotlyJS.attr(args = [PlotlyJS.attr(line=[PlotlyJS.attr(color = col_morph[i],width=lwd_morph[i]) for i in 1:n_reach])],
                                 label  = "Morph",
                                 method = "restyle"))
    buttons = vcat(buttons,
                   PlotlyJS.attr(args = [PlotlyJS.attr(line=[PlotlyJS.attr(color = col_pot[i],width=lwd_reach) for i in 1:n_reach])],
                                 label  = "EcoImp",
                                 method = "restyle"))
              
    PlotlyJS.relayout!(
        p,
        updatemenus = [PlotlyJS.attr(type       = "buttons",
                                     direction  = "left",
                                     buttons    = buttons,
                                     showactive = true,
                                     x          = 0.70,
                                     xanchor    = "left",
                                     y          = 1.19,
                                     yanchor    = "top"),])

    # add annotation:
    # ---------------

    annotations = String[]
    digits = 2
    text = ""
    for j in 1:n_crit
        if j > 1; text = string(text," | "); end
        text = string(text,string(barrier_labels[j],": ",
                                  "a_ups=",round(attrib[j]["att"]["a_ups"],digits=digits)," ",
                                  "a_int=",round(attrib[j]["att"]["a_int"],digits=digits)," ",
                                  " (",maximum(attrib[j]["regions_inv"])," reg)"))
    end
    annotations = vcat(annotations,
                       PlotlyJS.attr(text      = text,
                                     showarrow = false,
                                     x         = x_lim[1],
                                     # y         = y_lim[2]+1.05*(y_lim[2]-y_lim[1]), 
                                     y         = 1.08, 
                                     yref      = "paper", 
                                     align     = "left",
                                     font      = PlotlyJS.attr(size=15)))
    if plot_reach_ids
        for i in 1:n_reach
            annotations = vcat(annotations,
                               PlotlyJS.attr(text      = net["attrib_reach"][i,"Reach_ID"],
                                             showarrow = false,
                                             x         = 0.5*(net["attrib_reach"][i,"x_start"] +
                                                              net["attrib_reach"][i,"x_end"]),
                                             y         = 0.5*(net["attrib_reach"][i,"y_start"] +
                                                              net["attrib_reach"][i,"y_end"]),
                                             align     = "center",
                                             font      = PlotlyJS.attr(size=10)))
        end
    end

    PlotlyJS.relayout!(p,annotations=annotations)

    # Save plot and removed barriers and culverts:
    # --------------------------------------------

    if ! isdir(dir_out); mkdir(dir_out); end
    # does not work on some machines:
    # PlotlyJS.savefig(p,string(dir_out,"/",title," ",plot_id,"_plot.pdf"))
    if (length(barriers_removed)>0) | (length(culverts_removed)>0)
        CSV.write(string(dir_out,"/",title," ",plot_id,"_barriers_removed.csv"),
                  DataFrame(barrier = barriers_removed,
                            x       = net["attrib_node"][barriers_removed,"x"],
                            y       = net["attrib_node"][barriers_removed,"y"]),
                            delim   = ";")
        CSV.write(string(dir_out,"/",title," ",plot_id,"_culverts_removed.csv"),
                  DataFrame(culvert = culverts_removed,
                            id      = net["attrib_reach"][culverts_removed,"Reach_ID"],
                            x_start = net["attrib_reach"][culverts_removed,"x_start"],
                            x_end   = net["attrib_reach"][culverts_removed,"x_end"],
                            y_start = net["attrib_reach"][culverts_removed,"y_start"],
                            y_end   = net["attrib_reach"][culverts_removed,"y_end"]),
                            delim   = ";")
    end

    # add response to click and selection:
    # ------------------------------------

    WebIO.on(p["click"]) do data

        if haskey(data,"points")

            barriers_selected = Int[]
            for j in 1:n_crit
                if ( data["points"][1]["curveNumber"] == curvenum_barrier[j] )
                    barriers_selected = vcat(barriers_selected,ind_art[j][data["points"][1]["pointIndex"]+1])
                end
            end
            barriers_removed_modified = sort(unique(vcat(barriers_removed_local,barriers_selected)))

            culverts_selected = Int[]
            if n_culverts > 0
                for j in 1:n_culverts
                    if ( data["points"][1]["curveNumber"] == curvenum_culvert[j] )
                        culverts_selected = vcat(culverts_selected,ind_culvert[j])
                    end
                end
            end
            culverts_removed_modified = sort(unique(vcat(culverts_removed_local,culverts_selected)))
        
            NewWindow(barriers_removed_modified,culverts_removed_modified)

        end

    end

    WebIO.on(p["selected"]) do data

        if haskey(data,"points")

            barriers_selected = Int[]
            for j in 1:n_crit
                for i in 1:length(data["points"])
                    if ( data["points"][i]["curveNumber"] == curvenum_barrier[j] )
                        barriers_selected = vcat(barriers_selected,ind_art[j][data["points"][i]["pointIndex"]+1])
                    end
                end
            end
            barriers_removed_modified = sort(unique(vcat(barriers_removed_local,barriers_selected)))
        
            culverts_selected = Int[]
            if n_culverts > 0
                for j in 1:n_culverts
                    for i in 1:length(data["points"])
                        if ( data["points"][i]["curveNumber"] == curvenum_culvert[j] )
                            culverts_selected = vcat(culverts_selected,ind_culvert[j])
                        end
                    end
                end
            end
            culverts_removed_modified = sort(unique(vcat(culverts_removed_local,culverts_selected)))
    
            NewWindow(barriers_removed_modified,culverts_removed_modified)

        end

    end

    function NewWindow(barriers_removed_modified,culverts_removed_modified)

        barrier_art_modified = deepcopy(barrier_art_local)  # copy for modifications (array of different barrier types)
        for j in 1:n_crit
            if length(barriers_removed_modified) > 0
                barrier_art_modified[j][barriers_removed_modified] .= false
            end
        end

        culvert_modified = copy(culvert_local)  # copy for modifications
        if length(culverts_removed_modified) > 0
            culvert_modified[culverts_removed_modified] .= false
        end

        subid = subid + 1
        w = rivermanagement_RemoveBarriersCulverts(
                net,                   # net
                barrier_nat,           # barrier_nat
                barrier_art_modified,  # barrier_art
                toosteep,              # too steep reaches
                culvert_modified,      # culvert
                crit_name        = crit_name,
                thresh_length    = thresh_length,
                reach_morph      = reach_morph_local,
                reach_imp        = reach_imp_local,
                col_length       = col_length,
                col_width        = col_width,
                col_order        = col_order,
                size_x           = size_x,
                plot_id          = string(plot_id,"-",subid),
                title            = title,
                dir_out          = dir_out,
                plot_reach_ids   = plot_reach_ids,
                barriers_removed = barriers_removed_modified,
                culverts_removed = culverts_removed_modified)

    end

    # display, save and return plot:
    # ------------------------------

    w = Blink.Window()
    Blink.body!(w,WebIO.dom"div"(p))

    return w

end


function rivermanagement_SelectReaches(net::Dict;                  # rivernet
                                       plot_id          = "1",
                                       size_x           = 1000,
                                       title            = "",
                                       dir_out          = ".",
                                       plot_reach_ids   = false,
                                       reaches_selected = Int[])
       
    # check input and initialize variables:
    # -------------------------------------

    lwd_reach   = 3.0

    n_reach     = nrow(net["attrib_reach"])

    subid = 0

    col_reach = fill("blue",n_reach)
    if length(reaches_selected) > 0
        col_reach[reaches_selected] .= "red"
    end

    reaches_selected_local = copy(sort(unique(reaches_selected))) # keep original for later events

    # define layout and array for traces:
    # -----------------------------------

    t = string(title," ",plot_id)
    if length(reaches_selected_local) > 0
        t = string(t," (",length(reaches_selected_local)," reaches selected)")
    end

    x_lim = net["x_lim"]
    y_lim = net["y_lim"] 
    if x_lim[2] == x_lim[1]
        if y_lim[2] == y_lim[1]
            x_lim = [x_lim[1]-1.0,x_lim[2]+1.0] 
            y_lim = [y_lim[1]-1.0,y_lim[2]+1.0] 
        else
            x_lim = [x_lim[1]-0.1*(y_lim[2]-y_lim[1]),x_lim[2]+0.1*(y_lim[2]-y_lim[1])]
        end
    elseif y_lim[2] == y_lim[1]
        y_lim = [y_lim[1]-0.1*(x_lim[2]-x_lim[1]),y_lim[2]+0.1*(x_lim[2]-x_lim[1])]
    end
    layout = PlotlyJS.Layout(title      = t,
                             showlegend = false,
                             autosize   = false,
                             width      = size_x,          # seems not to work
                             height     = round(Int,size_x*(y_lim[2]-y_lim[1])/(x_lim[2]-x_lim[1])))

    traces = PlotlyJS.PlotlyBase.AbstractTrace[]

    # define reach traces:
    # --------------------

    for i in 1:n_reach

        local trace = PlotlyJS.scatter(x          = net["attrib_reach"][i,"coord"].x,
                                       y          = net["attrib_reach"][i,"coord"].y,
                                       mode       = "lines",
                                       line       = PlotlyJS.attr(color=col_reach[i],width=lwd_reach),
                                       showlegend = false)

        traces = vcat(traces,trace)

    end

    # add reach traces as scatter trace to allow for selection:
    # ---------------------------------------------------------

    curvenum_reach = zeros(Int,n_reach)
    for j in 1:n_reach
        curvenum_reach[j] = length(traces)
        trace = PlotlyJS.scatter(x          = net["attrib_reach"][j,"coord"].x,
                                 y          = net["attrib_reach"][j,"coord"].y,
                                 mode       = "markers",
                                 marker     = PlotlyJS.attr(color  = "black",
                                                            symbol = "circle",
                                                            line   = PlotlyJS.attr(width=0), # no border
                                                            size   = 1),
                                 showlegend = false)
        traces = vcat(traces,trace)
    end

    # plot river network:
    # -------------------

    p = PlotlyJS.plot(traces,layout)

    # add annotation:
    # ---------------

    # annotations = String[]
    # digits = 2
    # text = ""
    # for j in 1:n_crit
    #     if j > 1; text = string(text," | "); end
    #     text = string(text,string(barrier_labels[j],": ",
    #                               "a_ups=",round(attrib[j]["att"]["a_ups"],digits=digits)," ",
    #                               "a_int=",round(attrib[j]["att"]["a_int"],digits=digits)," ",
    #                               " (",maximum(attrib[j]["regions_inv"])," reg)"))
    # end
    # annotations = vcat(annotations,
    #                    PlotlyJS.attr(text      = text,
    #                                  showarrow = false,
    #                                  x         = x_lim[1],
    #                                  # y         = y_lim[2]+1.05*(y_lim[2]-y_lim[1]), 
    #                                  y         = 1.08, 
    #                                  yref      = "paper", 
    #                                  align     = "left",
    #                                  font      = PlotlyJS.attr(size=15)))
    # if plot_reach_ids
    #     for i in 1:n_reach
    #         annotations = vcat(annotations,
    #                            PlotlyJS.attr(text      = net["attrib_reach"][i,"Reach_ID"],
    #                                          showarrow = false,
    #                                          x         = 0.5*(net["attrib_reach"][i,"x_start"] +
    #                                                           net["attrib_reach"][i,"x_end"]),
    #                                          y         = 0.5*(net["attrib_reach"][i,"y_start"] +
    #                                                           net["attrib_reach"][i,"y_end"]),
    #                                          align     = "center",
    #                                          font      = PlotlyJS.attr(size=10)))
    #     end
    # end

    # PlotlyJS.relayout!(p,annotations=annotations)

    # Save plot and selected reaches:
    # -------------------------------

    if ! isdir(dir_out); mkdir(dir_out); end
    # does not work on some machines:
    # PlotlyJS.savefig(p,string(dir_out,"/",title," ",plot_id,"_plot.pdf"))
    if length(reaches_selected) > 0
        CSV.write(string(dir_out,"/",title," ",plot_id,"_reaches_selected.csv"),
                  DataFrame(reach   = reaches_selected,
                            id      = net["attrib_reach"][reaches_selected,"Reach_ID"],
                            x_start = net["attrib_reach"][reaches_selected,"x_start"],
                            x_end   = net["attrib_reach"][reaches_selected,"x_end"],
                            y_start = net["attrib_reach"][reaches_selected,"y_start"],
                            y_end   = net["attrib_reach"][reaches_selected,"y_end"]),
                            delim   = ";")
    end

    # add response to click and selection:
    # ------------------------------------

    WebIO.on(p["click"]) do data

        if haskey(data,"points")

            reaches_selected = Int[]
            for j in 1:n_reach
                if ( data["points"][1]["curveNumber"] == curvenum_reach[j] )
                    reaches_selected = vcat(reaches_selected,j)
                end
            end
            reaches_selected_modified = sort(unique(vcat(reaches_selected_local,reaches_selected)))
        
            NewWindow(reaches_selected_modified)

        end

    end

    WebIO.on(p["selected"]) do data

        if haskey(data,"points")
        
            reaches_selected = Int[]
            for j in 1:n_reach
                for i in 1:length(data["points"])
                    if ( data["points"][i]["curveNumber"] == curvenum_reach[j] )
                        reaches_selected = vcat(reaches_selected,j)
                    end
                end
            end
            reaches_selected_modified = sort(unique(vcat(reaches_selected_local,reaches_selected)))

            NewWindow(reaches_selected_modified)

        end

    end

    function NewWindow(reaches_selected_modified)

        subid = subid + 1
        w = rivermanagement_SelectReaches(net,                   # net
                                          plot_id          = string(plot_id,"-",subid),
                                          size_x           = size_x,
                                          title            = title,
                                          dir_out          = dir_out,
                                          plot_reach_ids   = plot_reach_ids,
                                          reaches_selected = reaches_selected_modified)

    end

    # display, save and return plot:
    # ------------------------------

    w = Blink.Window()
    Blink.body!(w,WebIO.dom"div"(p))

    return w

end
