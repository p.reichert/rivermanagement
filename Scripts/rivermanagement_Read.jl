## -------------------------------------------------------
##
## File: rivermanagement_Read.jl
##
## August 14, 2022 -- Peter Reichert
## peter.reichert@emeriti.eawag.ch
##
## -------------------------------------------------------

using  DataFrames
using  Statistics
import CSV


function rivermanagement_Read(file_reachcoord,
                              file_reachattrib = "",
                              file_nodeattrib  = "";
                              colnames         = (reach = "Reach_ID",
                                                  x     = "X",
                                                  y     = "Y",
                                                  z     = "Z"),
                              sep              = '\t',
                              tol              = 1.0,
                              outlet_reach     = Vector{Int}(),
                              verbose          = true)

    # check arguments:
    # ================

    if ! ( haskey(colnames,:reach) & haskey(colnames,:x) & haskey(colnames,:y) & haskey(colnames,:z) )
        println("rivermanagement_Read: *** argument \"colnames\" needs keys reach, x, y and z ***")
        return missing
    end
    if ! ( tol > 0.0 )
        println("rivermanagement_Read: *** argument \"tol\" needs to be larger than zero ***")
        return missing
    end

    # read input files:
    # =================

    # read reach coordinates:
    # -----------------------

    if ! isfile(file_reachcoord)
        println("rivermanagement_Read: *** file \"",file_reachcoord,"\" passed as first argument not found ***")
        return missing
    end
    if verbose
        println("rivermanagement_Read: reading data ...")
    end
    reach_coord  = CSV.read(file_reachcoord , DataFrame; delim=sep)
    if nrow(reach_coord) < 1
        println("rivermanagement Read: *** no coordinate records found on file \"",file_reachcoord,"\" ***")
        return missing
    end
    missing_cols = ""
    if ! in(colnames.reach,names(reach_coord))
        missing_cols = string(missing_cols," ",colnames.reach)
    end
    if ! in(colnames.x,names(reach_coord))
        missing_cols = string(missing_cols," ",colnames.x)
    end
    if ! in(colnames.y,names(reach_coord))
        missing_cols = string(missing_cols," ",colnames.y)
    end
    if length(missing_cols) > 0
        println("rivermanagement_Read: *** missing columns in file \"",file_reachcoord,"\": ",missing_cols," ***")
        return(missing)
    end
    if ( any(ismissing(reach_coord[:,colnames.x])) | any(ismissing(reach_coord[:,colnames.y])) )
        println("rivermanagement_Read: *** columns for x and y coordinates in file \"",file_reachcoord,"\" cannot have missing values")
        return(missing)
    end
    if ! in(colnames.z,names(reach_coord))
        reach_coord[:,colnames.z] = Array{Union{Mising,Float64},1}(missing,nrow(reach_coord))
    end
    reach_ids = unique(reach_coord[!,colnames.reach])
    n_reach   = length(reach_ids)
    if verbose
        println("  ",nrow(reach_coord)," reach coordinate records read")
    end

    # read reach attributes:
    # ----------------------

    attrib_reach_read = missing
    if isfile(file_reachattrib) 
        attrib_reach_read = CSV.read(file_reachattrib, DataFrame; delim=sep)
        missing_cols = ""
        if ! in(colnames.reach,names(attrib_reach_read))
            missing_cols = string(missing_cols," ",colnames.reach)
        end
        if length(missing_cols) > 0
            println("rivermanagement_Read: *** missing columns in file \"",file_reachattrib,"\": ",missing_cols," ***")
            return(missing)
        end
        if verbose
            println("  ",nrow(attrib_reach_read)," reach attribute records read")
        end
    else
        if verbose
            if length(file_reachattrib) > 0
                println("rivermanagement_Read: *** file \"",file_reachattrib,"\" passed as second argument not found ***")
            end
            println("  no reach attributes read")
        end
    end

    # read node attributes:
    # ---------------------

    attrib_node_read  = missing
    if isfile(file_nodeattrib)
        attrib_node_read  = CSV.read(file_nodeattrib , DataFrame; delim=sep)
        missing_cols = ""
        if ! in(colnames.x,names(attrib_node_read))
            missing_cols = string(missing_cols," ",colnames.x)
        end
        if ! in(colnames.y,names(attrib_node_read))
            missing_cols = string(missing_cols," ",colnames.y)
        end
        if length(missing_cols) > 0
            println("rivermanagement_Read: *** missing columns in file \"",file_nodeattrib,"\": ",missing_cols," ***")
            return(missing)
        end
        if ( any(ismissing(reach_coord[:,colnames.x])) | any(ismissing(reach_coord[:,colnames.y])) )
            println("rivermanagement_Read: *** columns for x and y coordinates in file \"",file_nodeattrib,"\" cannot have missing values")
            return(missing)
        end
            if verbose
            println("  ",nrow(attrib_node_read)," node attribute records read")
        end
    else
        if verbose
            if length(file_nodeattrib) > 0
                println("rivermanagement_Read: *** file \"",file_nodeattrib,"\" passed as third argument not found ***")
            end
            println("  no node attributes read")
        end
    end

    # get extent:
    # ===========

    x_min = minimum(reach_coord[!,colnames.x])
    x_max = maximum(reach_coord[!,colnames.x])
    y_min = minimum(reach_coord[!,colnames.y])
    y_max = maximum(reach_coord[!,colnames.y])
    z_min = missing
    z_max = missing
    if any( .! ismissing.(reach_coord[!,colnames.z]))
        z_min = minimum(skipmissing(reach_coord[!,colnames.z]))
        z_max = maximum(skipmissing(reach_coord[!,colnames.z]))
    end

    # extract coordinates and calculate lengths and slopes of reaches:
    # ================================================================

    x_start  = zeros(n_reach)
    x_end    = zeros(n_reach)
    y_start  = zeros(n_reach)
    y_end    = zeros(n_reach)
    z_start  = Array{Union{Missing,Float64},1}(missing,n_reach)
    z_end    = Array{Union{Missing,Float64},1}(missing,n_reach)
    len      = zeros(n_reach)
    slope    = Array{Union{Missing,Float64},1}(missing,n_reach)
    reaches  = Vector{NamedTuple}()
    id_point = Vector{Any}()
    for (i,id) in enumerate(reach_ids)
        ind = findall(x->x==id,reach_coord[!,colnames.reach])
        if length(ind) < 2
            push!(id_point,reach_ids[i])
        end
        x = reach_coord[ind,colnames.x]
        x_start[i] = x[1]
        x_end[i]   = x[end]
        y = reach_coord[ind,colnames.y]
        y_start[i] = y[1]
        y_end[i]   = y[end]
        z = reach_coord[ind,colnames.z]
        z_start[i] = z[1]
        z_end[i]   = z[end]
        len[i] = 0
        for j in 2:length(x)
            len[i] += sqrt((x[j]-x[j-1])^2+(y[j]-y[j-1])^2)
        end
        if ( ! ismissing(z_start[i]) ) & ( ! ismissing(z_end[i]))
            slope[i] = abs(z_end[i]-z_start[i])/len[i]
        end
        reaches = vcat(reaches,(x=x,y=y,z=z)) 
    end
    if length(id_point) > 0
        println("rivermanagement_Read: *** there are reaches consisting of a single point: ",join(id_point,',')," ***")
        return missing
    end

    # identify nodes:
    # ===============

    dist2 = zeros(n_reach)
    id_zero = Vector{Any}()
    for i in 1:n_reach 
        dist2[i] = (x_end[i]-x_start[i])^2 + (y_end[i]-y_start[i])^2
        if dist2[i] == 0
            push!(id_zero,reach_ids[i])
        end
    end
    if length(id_zero) > 0
        println("rivermanagement_Read: *** there are reaches ending at their start value: ",join(id_zero,',')," ***")
        return missing
    end

    tol2 = minimum([tol^2,0.01*minimum(dist2)])  # at least 10% of min. dist. start-end
    n_node = 2
    node_start = 1
    node_end   = 2
    if n_reach > 1
        node_start = zeros(Int,n_reach)
        node_end   = zeros(Int,n_reach)
        n_node     = 0
        for i in 1:n_reach
            if node_start[i] == 0  # node is not node of reach with index < i: new node
                n_node += 1
                node_start[i] = n_node
            end
            if node_end[i] == 0    # node is not node of reach with index < i: new node
                n_node += 1
                node_end[i] = n_node
            end
            if i < n_reach  # search whether start or end node of reach i is also node of a
                            # reach with index > i; if yes, assign this node the same index
                dist2 = (x_start[i] .- x_start[(i+1):n_reach]) .* 
                          (x_start[i] .- x_start[(i+1):n_reach]) + 
                        (y_start[i] .- y_start[(i+1):n_reach]) .*
                          (y_start[i] .- y_start[(i+1):n_reach])
                if sum(dist2 .< tol2) > 0
                    offset = findall(x->x<tol2,dist2)
                    node_start[i.+offset] .= node_start[i]
                end
                dist2 = (x_start[i] .- x_end[(i+1):n_reach]) .* 
                          (x_start[i] .- x_end[(i+1):n_reach]) + 
                        (y_start[i] .- y_end[(i+1):n_reach]) .*
                          (y_start[i] .- y_end[(i+1):n_reach])
                if sum(dist2 .< tol2) > 0
                    offset = findall(x->x<tol2,dist2)
                    node_end[i.+offset] .= node_start[i]
                end
                dist2 = (x_end[i] .- x_start[(i+1):n_reach]) .* 
                          (x_end[i] .- x_start[(i+1):n_reach]) + 
                        (y_end[i] .- y_start[(i+1):n_reach]) .*
                          (y_end[i] .- y_start[(i+1):n_reach])
                if sum(dist2 .< tol2) > 0
                    offset = findall(x->x<tol2,dist2)
                    node_start[i.+offset] .= node_end[i]
                end
                dist2 = (x_end[i] .- x_end[(i+1):n_reach]) .* 
                          (x_end[i] .- x_end[(i+1):n_reach]) + 
                        (y_end[i] .- y_end[(i+1):n_reach]) .*
                          (y_end[i] .- y_end[(i+1):n_reach])
                if sum(dist2 .< tol2) > 0
                    offset = findall(x->x<tol2,dist2)
                    node_end[i.+offset] .= node_end[i]
                end
            end
        end
    end

    # construct internal reach attributes:
    # ====================================

    attrib_reach = DataFrame(Reach_ID    = reach_ids,
                             coord       = reaches,
                             x_start     = x_start,
                             y_start     = y_start,
                             z_start     = z_start,
                             x_end       = x_end,
                             y_end       = y_end,
                             z_end       = z_end,
                             node_start  = node_start,
                             node_end    = node_end,
                             length      = len,
                             slope       = slope)

    # construct internal node attributes:
    # ===================================

    x_node = zeros(n_node)
    y_node = zeros(n_node)
    z_node = Array{Union{Missing,Float64},1}(missing,n_node)
    for i in 1:n_node
        ind_start = findall(x->x==i,node_start)
        ind_end   = findall(x->x==i,node_end)
        x_node[i] = mean(vcat(x_start[ind_start],x_end[ind_end]))
        y_node[i] = mean(vcat(y_start[ind_start],y_end[ind_end]))
        if any(.! ismissing.(vcat(z_start[ind_start],z_end[ind_end])))
            z_node[i] = mean(skipmissing(vcat(z_start[ind_start],z_end[ind_end])))
        end
    end

    attrib_node = DataFrame(x = x_node,
                            y = y_node)
    if ! ismissing(z_min)
        attrib_node.z = z_node
    end

    # merge internal and external reach attributes:
    # =============================================

    if ! ismissing(attrib_reach_read)
        # replace reach identifier on input data by the one used internally:
        rename!(attrib_reach_read,colnames.reach => "Reach_ID")
        # remove duplicate records:
        ids = unique(attrib_reach_read[!,"Reach_ID"])
        inds = [findfirst(x -> x==ids[i],attrib_reach_read[!,"Reach_ID"]) for i in 1:length(ids)]
        attrib_reach_read = attrib_reach_read[inds,:]
        # remove columns from input data that have a name already present in attrib_reach 
        # except for the reach identifier
        colnames_used = names(attrib_reach)
        for i in 2:length(colnames_used)
            if in(colnames_used[i],names(attrib_reach_read))
                select!(attrib_reach_read,Not(colnames_used[i]))
            end
        end
        # join the data frames:
        attrib_reach = leftjoin(attrib_reach,attrib_reach_read,on=:Reach_ID)
    end

    # merge internal and external node attributes:
    # ============================================

    if ! ismissing(attrib_node_read)
        # add temporary node ids for joining:
        insertcols!(attrib_node,1,:nodeind_tmp=>1:n_node)
        # add temporary column to write node ids:
        insertcols!(attrib_node_read,1,:nodeind_tmp=>zeros(nrow(attrib_node_read)))
        # search for presence of node data and write node id if found:
        found = false
        for i in 1:n_node
            dist2 = (x_node[i].-attrib_node_read[!,colnames.x]) .* (x_node[i].-attrib_node_read[!,colnames.x]) .+ 
                    (y_node[i].-attrib_node_read[!,colnames.y]) .* (y_node[i].-attrib_node_read[!,colnames.y])
            m = findmin(dist2)
            if ( m[1] < tol2 )
                attrib_node_read[m[2],:nodeind_tmp] = i
                found = true
            end
        end
        # join internal with external node data:
        if found
            attrib_node = leftjoin(attrib_node,attrib_node_read,on=:nodeind_tmp)
            permind = sortperm(attrib_node.nodeind_tmp)
            attrib_node = attrib_node[permind,:]
        end
        # remove temporary node ids:
        attrib_node = attrib_node[:,2:ncol(attrib_node)]
    end

    # identify subnets:
    # =================

    # subnets consist of reaches that have a common node with other reaches

    subnet = zeros(Int,n_reach)   # indices of subnet association of all reaches
    n_subnet = 0                  # number of subnets
    for i in 1:n_reach
        if subnet[i] == 0
            # start new subnet:
            n_subnet += 1
            # new reach in subnet:
            reaches = [i]
            while length(reaches) > 0
                # fill subnet for current reaches:
                setindex!(subnet,fill(n_subnet,length(reaches)),reaches)
                # get nodes of current reaches:
                nodes = unique(vcat(node_start[reaches],node_end[reaches]))
                # get reaches that contain these nodes:
                reaches = unique(vcat(findall(x->x in nodes,node_start),findall(x->x in nodes,node_end)))
                # focus on new reaches:
                if length(reaches) > 0
                    reaches = reaches[findall(x->subnet[x]==0,reaches)]
                end 
            end
        end
    end
    subnet_lengths = zeros(n_subnet)
    for i in 1:n_subnet; subnet_lengths[i] = sum(len[findall(isequal(i),subnet)]); end
    perm    = sortperm(subnet_lengths,rev=true)
    perm2   = sortperm(perm)
    subnet  = perm2[subnet]
    subnet_lengths = subnet_lengths[perm]

    # update rivernet structure and write summary output:
    attrib_reach.subnet = subnet

    # identify end nodes and end reaches:
    # ===================================

    # end reachs have a node that is not a node of any other reach

    n_start  = zeros(Int,n_reach)         # number of reaches connecting to the start of each reach
    n_end    = zeros(Int,n_reach)         # number of reaches connecting to the end of each reach
    endreach = fill(false,n_reach)        # logical vector flagging end reaches
    nodes    = vcat(node_start,node_end)  # all nodes (including multiple instances from different reaches)
    for i in 1:n_reach
        # calculate number of reaches that contain the start node of the current reach:
        n_start[i] = sum(node_start[i] .== nodes) - 1
        # calculate number of reaches that contain the end node of the current reach:
        n_end[i]   = sum(node_end[i]   .== nodes) - 1
        # if there are no connecting reaches to either start or end node, the reach is an endreach:
        if (n_start[i] == 0) | (n_end[i] == 0)
            endreach[i] = true
        end
    end
    # update rivernet structure and write summary output:
    attrib_reach.n_start  = n_start
    attrib_reach.n_end    = n_end
    attrib_reach.endreach = endreach

    # find outlets:
    # =============

    # criteria: 
    # 1. outlet index provided by user (as function argument)
    # 2. if elevation is provided, use lowest end reach with lowest elevation node
    # 3. if coordinate direction is unique, use reach with unique start or end node

    outlet = fill(false,n_reach)
    # search unique outlet for each subnet:
    for s in 1:n_subnet
        outlet_s = 0
        endreach_s = findall((subnet.==s) .& endreach) 
        if ( length(endreach_s) < 1 )  # no endreach -> no outlet
            println("rivermanagement_Read: *** no potential outlet found for subnet ",s," (circular connections?) ***")
        else
            # check for outlet provided as an argument
            if length(outlet_reach) > 0 
                ind = findall(x->x in output_reach,endreach_s)
                if length(ind) == 1
                    outlet_s = endreach_s[ind[1]]
                elseif length(ind) > 1
                    println("rivermanagement_Read: *** multple outlets specified for subnet ",s," ***") # continue search below
                end
            end
            if outlet_s == 0
                # if elevation is provided use reach with lowest elevevation outlet node:
                z_endreach_s = ifelse.(n_start[endreach_s].==0,attrib_reach.z_start[endreach_s],attrib_reach.z_end[endreach_s])
                if any(.! ismissing.(z_endreach_s))
                    outlet_s = endreach_s[argmin(skipmissing(z_endreach_s))]
                else
                # otherwise find outlet based on unique coordinate direction:
                    if length(endreach_s) > 2  # otherwise coordinate direction does not help
                        start_open = findall(n_start[endreach_s]==0)
                        end_open   = findall(n_end[endreach_s]==0)
                        if length(end_open) == 1
                            outlet_s = endreach_s[end_open[1]]
                        elseif length(start_open) == 1
                            outlet_s = endreach_s[start_open[1]]
                        end
                    end
                end
            end
        end
        if outlet_s != 0   # outlet found
            outlet[outlet_s] = true
        else               # outlet not found
            #outlet[endreach_s] = fill(missing,length(endreach_s))
            println("rivermanagement_Read: *** outlet of subnet ",s," could not be found ***")
        end
    end
    headwater = endreach .& (.!outlet)
    # update rivernet structure and write summary output:
    attrib_reach.outlet    = outlet
    attrib_reach.headwater = headwater
    
    # write summary and return:
    # =========================
    
    if verbose
        println("  number of reaches:          ",n_reach)
        println("  number of nodes:            ",n_node)
        println("  number of subnets:          ",n_subnet)
        println("  number of outlets:          ",sum(outlet .== true)),
        println("  number of headwaters:       ",sum(headwater .== true))
        println("  number of end reaches:      ",sum(endreach))
        println("  number of internal reaches: ",n_reach - sum(endreach))
        println("  reach lengths:              ",round(minimum(len),sigdigits=6)," - ",round(maximum(len),sigdigits=6))
        println("  subnet lengths:             ",round.(subnet_lengths,sigdigits=6))
        println("  total network length:       ",round(sum(len),sigdigits=6))
    end

    net = Dict("n_reach"       => n_reach,
               "n_node"         => n_node,
               "attrib_reach"   => attrib_reach,
               "attrib_node"    => attrib_node,
               "x_lim"          => [x_min,x_max],
               "y_lim"          => [y_min,y_max],
               "z_lim"          => [z_min,z_max],
               "length"         => sum(attrib_reach.length),
               "n_subnet"       => n_subnet,
               "subnet_lengths" => subnet_lengths)

    return net
end

