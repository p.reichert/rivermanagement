## -------------------------------------------------------
##
## File: rivermanagement_Interactive.jl
##
## August 07, 2023 -- Peter Reichert
## peter.reichert@emeriti.eawag.ch
##
## -------------------------------------------------------


# options:
# ========

# define input and output directories:

dir_input       = "../Input"
dir_output      = "../Output"

remove_barriers = true  # if true, select culverts and nodes for rehabilitation,
                        # if false, select any reaches to get their indices

plot_reach_ids  = false

# select river:

# river = "Demo0a"
# river = "Demo0b"
# river = "Demo1"
# river = "Demo2"
# river = "Demo3"
# river = "Aa"
# river = "Suhre"
# river = "Toess"
# river = "Broye"
# river = "Kander"
# river = "Birs"
# river = "Zulg"
# river = "Gluetschbach"
# river = "Rotache"
# river = "Giesse"
# river = "Chise"
river = "Guerbe"

# define parameters:

crit_label      = ["nons","salm"]
crit_height_nat = [50.5,80.5]
crit_height_art = [20.5,50.5]
crit_slope      = [ 0.1, 0.2]
thresh_length   = 30.0

# define column names of attributes:

col_reach_covered = "EINDOL"      # 0: not covered; 1: covered
col_reach_width   = "GSBREITE"    # width of river bed
col_reach_order   = "FLOZ"        # stream order
col_node_type     = "BarrierType" # 0: no drop; 1: natural drop; 2: artificial drop or construction
col_node_height   = "Height"      # drop height in cm
col_reach_length  = "length"      # length of river reach
col_reach_slope   = "slope"       # mean slope of river reach
col_reach_morph   = "val_morph"   # morphological state [0,1]; good if >= 0.6
col_reach_imp     = "ecol_imp"    # ecological importance; default 1, factor > 1 for higher potential (< 1 for lower)


# import libraries:
# =================

import Pkg
Pkg.activate(".")
Pkg.instantiate()

rivermanagement_Plotlib = "Plotly"
include("rivermanagement.jl")


# read and analyze river network:
# ===============================

# reach_coord  = CSV.read(string(dir_input,"/",river,"_Reach_Coord.csv"),  DataFrame; delim=';')
# reach_attrib = CSV.read(string(dir_input,"/",river,"_Reach_Attrib_val_morph.csv"), DataFrame; delim=';')
# node_attrib  = CSV.read(string(dir_input,"/",river,"_Node_Attrib.csv"),  DataFrame; delim=';')

@time rivernet = rivermanagement_Read(string(dir_input,"/",river,"_Reach_Coord.csv"),
                                      string(dir_input,"/",river,"_Reach_Attrib_val_morph.csv"),
                                      string(dir_input,"/",river,"_Node_Attrib.csv"),
                                      sep     = ';')
if ismissing(rivernet); error(string("*** unable to read river net for river ",river,)); end


# derive properties:
# ==================

n_node  = nrow(rivernet["attrib_node"])
n_reach = nrow(rivernet["attrib_reach"])

nodetype = rivernet["attrib_node"][:,col_node_type]  # 0: no drop; 1: natural drop; 2: artificial drop
replace!(nodetype,missing=>0)                        # replace missing information by no drop
nodeheight = rivernet["attrib_node"][:,col_node_height]  # drop height in cm
replace!(nodeheight,missing=>0.0)                    # replace missing information by zero height

covered = rivernet["attrib_reach"][:,col_reach_covered]
replace!(covered,missing=>0)                         # replace missing information by not covered
replace!(covered,9=>0)                               # replace 9 by not covered ??
culvert     = [false,true][covered.+1]
replace!(culvert,missing=>false)
val_morph   = rivernet["attrib_reach"][:,col_reach_morph]
val_morph_missing1 = copy(val_morph)
replace!(val_morph_missing1,missing=>1.0)
if col_reach_imp in names(rivernet["attrib_reach"])
    val_imp = rivernet["attrib_reach"][:,col_reach_imp]
    replace!(val_imp,missing=>1.0)
else
    val_imp = fill(1.0,n_reach)
end

barrier_nat = Vector{Vector{Bool}}(undef,length(crit_label))
barrier_art = Vector{Vector{Bool}}(undef,length(crit_label))
toosteep    = Vector{Vector{Bool}}(undef,length(crit_label))
for i in 1:length(crit_label)
    barrier_nat[i] = (nodetype .== 1) .&& (nodeheight .> crit_height_nat[i])
    barrier_art[i] = (nodetype .== 2) .&& (nodeheight .> crit_height_art[i])
    slope = copy(rivernet["attrib_reach"][:,col_reach_slope])
    replace!(slope,missing=>0.0)
    toosteep[i] = slope .> crit_slope[i]
end


# start first interactive plot:
# =============================

if remove_barriers

    # natural state:

    w0 = rivermanagement_RemoveBarriersCulverts(
            rivernet,                                  # net
            barrier_nat,                               # barrier_nat
            [fill(false,n_node),fill(false,n_node)],   # barrier_art
            toosteep,                                  # too steep regions
            fill(false,n_reach),                       # culvert
            crit_name      = crit_label,
            thresh_length  = thresh_length,
            reach_morph    = fill(1.0,n_reach),
            reach_imp      = val_imp,
            col_length     = col_reach_length,
            col_width      = col_reach_width,
            col_order      = col_reach_order,
            plot_id        = "",
            title          = string(river," Natural State"),
            dir_out        = "../Protocol",
            plot_reach_ids = plot_reach_ids)

    # current state:

    w  = rivermanagement_RemoveBarriersCulverts(
            rivernet,                                  # net
            barrier_nat,                               # barrier_nat
            barrier_art,                               # barrier_art
            toosteep,                                  # too steep regions
            culvert,                                   # culvert
            crit_name      = crit_label,
            thresh_length  = thresh_length,
            reach_morph    = val_morph,
            reach_imp      = val_imp,
            col_length     = col_reach_length,
            col_width      = col_reach_width,
            col_order      = col_reach_order,
            title          = river,
            dir_out        = "../Protocol",
            plot_reach_ids = plot_reach_ids)

else

    w  = rivermanagement_SelectReaches(
            rivernet,                                  # net
            title          = river,
            dir_out        = "../Protocol",
            plot_reach_ids = plot_reach_ids)

end