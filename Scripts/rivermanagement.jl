## -------------------------------------------------------
##
## File: rivermanagement.jl
##
## August 14, 2022 -- Peter Reichert
## peter.reichert@emeriti.eawag.ch
##
## -------------------------------------------------------

using  DataFrames
using  Statistics
import CSV

include("rivermanagement_Read.jl")
include("rivermanagement_Write.jl")
include("rivermanagement_Analyze.jl")
include("rivermanagement_UpstreamConnectivity.jl")
include("rivermanagement_ConnectedRegions.jl")
include("rivermanagement_CalcAttributes.jl")
include("rivermanagement_Auxiliaries.jl")

if ! @isdefined rivermanagement_Plotlib; rivermanagement_Plotlib = "Plotly"; end
if rivermanagement_Plotlib == "Plots"
   include("rivermanagement_Plot_Plots.jl")
else
   include("rivermanagement_Plot_Plotly.jl")
   include("rivermanagement_SelectInteractively.jl")
end


