## -------------------------------------------------------
##
## File: rivermanagement_Analyze.jl
##
## August 14, 2022 -- Peter Reichert
## peter.reichert@emeriti.eawag.ch
##
## -------------------------------------------------------

using  DataFrames
import CSV
import Statistics

function rivermanagement_Analyze(net;
                                 verbose      = true)

    # definitions:
    # ============

    n_reach    = net["n_reach"]
    n_node     = net["n_node"]
    node_start = net["attrib_reach"][:,"node_start"]
    node_end   = net["attrib_reach"][:,"node_end"]
    n_subnet   = net["n_subnet"]
    subnet     = net["attrib_reach"][:,"subnet"]
    n_start    = net["attrib_reach"][:,"n_start"]
    n_end      = net["attrib_reach"][:,"n_end"]
    outlet     = net["attrib_reach"][:,"outlet"]
    headwater  = net["attrib_reach"][:,"headwater"]

    if verbose
        println("rivermanagement_Analyze: analyzing network ...")
    end

    # identify downstream direction (by moving upstream from outlet):
    # ===============================================================

    downstream = Array{Union{Missing,Bool}}(missing,n_reach)
    reach_down = fill(0,n_reach)
    for s in 1:n_subnet
        reach_s  = findall(subnet.==s)
        outlet_s = findall((subnet.==s) .& outlet)
        if ( length(outlet_s) != 1 )
            println("rivermanagement_Analyze: *** flow direction analysis for subnet ",s," was not successful (no unique outlet identified) ***")
        else
            outlet_s = outlet_s[1]  # change type to scalar
            if n_end[outlet_s] == 0
                downstream[outlet_s] = true
            else
                downstream[outlet_s] = false
            end
            counter = 0
            to_visit = Vector{Int}()
            reach_current = outlet_s
            while true
                if downstream[reach_current]
                    node_upstream = node_start[reach_current]
                else
                    node_upstream = node_end[reach_current]
                end
                reaches_upstream = findall(node_start.==node_upstream)  # connected by start
                if ( length(reaches_upstream) > 0 )
                    for r in reaches_upstream
                        if r != reach_current
                            downstream[r] = false
                            push!(to_visit,r)
                            reach_down[r] = reach_current
                        end
                    end
                end
                reaches_upstream = findall(node_end.==node_upstream)  # connected by end
                if ( length(reaches_upstream) > 0 )
                    for r in reaches_upstream
                        if r != reach_current
                            downstream[r] = true
                            push!(to_visit,r)
                            reach_down[r] = reach_current
                        end
                    end
                end
                counter += 1
                if counter > n_reach
                    println("rivermanagement_Analyze: *** unable to complete network analysis in subnet ",s," (circular references?) ***")
                    break
                end
                if length(to_visit) == 0
                    break
                end
                reach_current = to_visit[1]
                deleteat!(to_visit,1)
            end
        end 
    end
    # update rivernet structure and write summary output:
    net["attrib_reach"].downstream = downstream
    net["attrib_reach"].reach_down = reach_down

    # establish linking:
    # ==================

    from_reach = Vector{Vector{Int}}(undef,n_node)
    for i in 1:n_node
        from_reach[i] = []
    end
    to_reach   = zeros(Int,n_node)
    from_node  = Vector{Union{Missing,Int}}(missing,n_reach)
    to_node    = Vector{Union{Missing,Int}}(missing,n_reach)
    for i in 1:n_reach
        if ! ismissing(downstream[i])
            if downstream[i]
                from_node[i] = node_start[i]
                to_node[i]   = node_end[i]
            else
                from_node[i] = node_end[i]
                to_node[i]   = node_start[i]
            end
            push!(from_reach[to_node[i]],i)
            to_reach[from_node[i]]   = to_reach[from_node[i]] = i
        end
    end
    # update rivernet structure:
    net["attrib_reach"].from_node = from_node
    net["attrib_reach"].to_node   = to_node
    net["attrib_node"].from_reach = from_reach
    net["attrib_node"].to_reach   = to_reach

    # find paths from headwaters to outlet:
    # =====================================

    reach_headwater = findall(headwater)
    n_headwater = length(reach_headwater)
    paths = Vector{Vector{Int}}(undef,n_headwater)
    for i in 1:n_headwater
        paths[i] = [reach_headwater[i]]
        reach_current = reach_headwater[i]
        counter = 0
        while true
            reach_current = reach_down[reach_current]
            if ! ( reach_current > 0 )
                break
            end
            push!(paths[i],reach_current)
            counter += 1
            if counter > n_reach
                println("rivermanagement_Analyze: *** problem identifying network path ***")
                break
            end
        end
    end    
    # update rivernet structure and write summary output:
    push!(net,"paths" => paths)
    if verbose
        println("  number of network paths:    ",n_headwater)
    end

    # determine stream order:
    # =======================
 
    streamorder = zeros(Int,n_reach)
    reach_headwater = findall(headwater)
    n_headwater = length(reach_headwater)
    for i in 1:n_headwater     # loop across headwaters
        reach_current = reach_headwater[i]
        order_current = 1
        streamorder[reach_current] = order_current
        while reach_down[reach_current] != 0
            node_down     = to_node[reach_current]
            reach_joining = from_reach[node_down]
            if length(reach_joining) > 1   # there is a joining reach
                reach_joining = reach_joining[findall(reach_joining.!=reach_current)]
                if sum(streamorder[reach_joining].==0) > 0
                    break   # order for joining reach not yet calculated; proceed with next headwater
                else
                    max_joining_order = maximum(streamorder[reach_joining])
                    if max_joining_order == order_current
                        order_current += 1
                    elseif max_joining_order > order_current
                        order_current = max_joining_order
                    end
                end
            end
            reach_current = reach_down[reach_current]
            streamorder[reach_current] = order_current
        end
    end
    # update rivernet structure and write summary output:
    net["attrib_reach"].streamorder = streamorder
    if verbose
        println("  maximum stream order:       ",maximum(streamorder))
    end

    return net
end
