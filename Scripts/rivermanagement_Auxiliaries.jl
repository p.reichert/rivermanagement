## -------------------------------------------------------
##
## File: rivermanagement_Auxiliaries.jl
##
## October 3, 2024 -- Peter Reichert
## peter.reichert@emeriti.eawag.ch
##
## -------------------------------------------------------


function rivermanagement_GetMSKColor(val)

    if ismissing(val); return missing; end

    if val > 0.8
        return "blue"
    elseif val > 0.6
        return "green"
    elseif val > 0.4
        return "yellow"
    elseif val > 0.2
        return "orange"
    else
        return "red"
    end

end


function rivermanagement_GetRegionColors(n)  # see http://juliagraphics.github.io/Colors.jl/stable/namedcolors/

    local cols = ["pink",
                  "green",
                  "brown",
                  "violet",
                  "coral",
                  "gold",
                  "blueviolet",
                  "olive",
                  "darkcyan",
                  "deepskyblue",
                  "moccasin",
                  "darkblue",
                  "grey",
                  "deeppink",
                  "limegreen",
                  "chocolate",
                  "magenta",
                  "goldenrod",
                  "violetred"]
    # culverts and too steep sections "black", region reachable from the outlet "cyan", other colors
    local col  = vcat(["black","cyan"],repeat(cols,round(Int,n/length(cols))+1))

    return col[1:n]

end


function rivermanagement_GetRehabCosts(rivernet,barriers_toremove,culverts_toremove,
                                       col_node_height,col_reach_width,col_reach_length,col_reach_order)

    costs_barrier_offset = 0.0500  # MFr
    costs_barrier_perm2  = 0.0150  # MFr/m2
    costs_culvert_rural  = 0.0015  # MFr/m
    costs_culvert_urban  = 0.0030  # MFr/m

    costs         = 0.0
    area          = 0.0
    culvertlength = 0.0

    # costs of barrier removal:

    if length(barriers_toremove) > 0
        heights = 0.01 .* rivernet["attrib_node"][!,col_node_height][barriers_toremove]   # cm to m conversion
        widths  = Vector{Union{Missing,Float64}}(missing,length(barriers_toremove))
        for j in 1:length(barriers_toremove)
            ind_adj = vcat(findall(isequal(barriers_toremove[j]),rivernet["attrib_reach"][!,"node_start"]),
                           findall(isequal(barriers_toremove[j]),rivernet["attrib_reach"][!,"node_end"]))
            w_adj = rivernet["attrib_reach"][ind_adj,col_reach_width]
            if !all(ismissing.(w_adj))
                widths[j] = maximum(skipmissing(w_adj))
            else
                error(string("*** rivermanagement_GetRehabCosts: width not available for barrier ",barriers_toremove[j]))
            end
        end
        areas = heights .* widths
        if length(findall(ismissing(areas))) == 0
            area  = sum(areas)
            costs = costs_barrier_offset + costs_barrier_perm2 * area
        else
            area  = missing
            costs = missing
        end
    end

    # costs of culvert removal:

    if (!ismissing(costs)) & (length(culverts_toremove) > 0)
        culvertlength = sum(rivernet["attrib_reach"][culverts_toremove,col_reach_length])
        costs = costs + 0.5*(costs_culvert_rural+costs_culvert_urban) * culvertlength  # no information about rural or urban environment yet
    end

    return (area,culvertlength,costs)

end