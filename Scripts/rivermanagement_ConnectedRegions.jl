## -------------------------------------------------------
##
## File: rivermanagement_ConnectedRegions.jl
##
## January 24, 2023 -- Peter Reichert
## peter.reichert@emeriti.eawag.ch
##
## -------------------------------------------------------


function rivermanagement_ConnectedRegions(net;
                                          crit_reach    = true,
                                          crit_node     = true,
                                          thresh_length = 0.0,
                                          verbose       = true)

    # check input:
    # ============

    if ! ( haskey(net,"attrib_reach") && haskey(net,"attrib_node") && haskey(net,"n_subnet") )
        println(string("rivermanagement_ConnectedRegions: *** argument \"net\" ",
                       "needs keys \"attrib_reach\", \"attrib_node\" and \"n_subnet\" ***"))
        return
    end
    if ! ( ( columnindex(net["attrib_reach"],"node_start") > 0 ) &
           ( columnindex(net["attrib_reach"],"node_end") > 0 ) &
           ( columnindex(net["attrib_reach"],"length") > 0 ) &
           ( columnindex(net["attrib_reach"],"subnet") > 0 ) &
           ( columnindex(net["attrib_reach"],"outlet") > 0 ) )
           println(string("rivermanagement_ConnectedRegions: *** element \"attrib_reach\" ",
                          "of argument \"net\" needs keys \"node_start\", \"node_end\" ",
                          "\"length\", \"subnet\" and \"outlet\" ***"))
        return
    end

    # initialize:
    # ===========

    n_reach    = nrow(net["attrib_reach"])
    n_node     = nrow(net["attrib_node"])
    n_subnet   = net["n_subnet"]
    node_start = net["attrib_reach"].node_start
    node_end   = net["attrib_reach"].node_end
    subnet     = net["attrib_reach"].subnet
    outlet     = net["attrib_reach"].outlet
    len        = net["attrib_reach"].length

    if (! isa(crit_node,Array)) && (! isa(crit_node,BitVector))
        allowed_nodes = fill(crit_node,n_node)
    elseif length(crit_node) == 1
        allowed_nodes = fill(crit_node[1],n_node)
    elseif length(crit_node) == n_node
        allowed_nodes = crit_node
    else
        println(string("rivermanagement_ConnectedRegions: *** argument \"crit_node\" ",
                       "has to be a bool scalar or vector of length 1 or number of nodes"))
    end
    if (! isa(crit_reach,Array)) && (! isa(crit_reach,BitVector))
        allowed_reaches = fill(crit_reach,n_reach)
    elseif length(crit_reach) == 1
        allowed_reaches = fill(crit_reach[1],n_reach)
    elseif length(crit_reach) == n_reach
        allowed_reaches = crit_reach
    else
        println(string("rivermanagement_ConnectedRegions: *** argument \"crit_reach\" ",
                       "has to be a bool scalar or vector of length 1 or number of reaches"))
    end

    # find connected regions:
    # =======================

    regions = zeros(Int,n_reach)
    region  = 0

    accumulated_length = Vector{Union{Missing,Float64}}(missing,n_node)

    # loop over subnets:

    for s in 1:n_subnet

        reaches_s = findall((subnet .== s) .& allowed_reaches)
        if length(reaches_s) > 0

            reach_current = findfirst((subnet.==s) .& outlet)
            if isnothing(reach_current) 
                reach_current = reaches_s[1]
            else
                if ! allowed_reaches[reach_current]
                    reach_current = reaches_s[1]
                end
            end

            # loop over regions:

            while(true)

                region = region + 1
    
                accumulated_length .= missing
    
                to_visit = Int[]

                # loop over reaches:

                while(true)
    
                    regions[reach_current] = region
    
                    # update accumulated lengths:
    
                    if allowed_reaches[reach_current]
                        accumulated_length[node_start[reach_current]] = 0.0
                        accumulated_length[node_end[reach_current]]   = 0.0
                    else
                        accumulated_length_start = 0.0
                        accumulated_length_end   = 0.0
                        if ismissing(accumulated_length[node_start[reach_current]])
                            # current reach had been reached from node_end:
                            #     node_end:   keep length
                            #     node_start: add length of reach to length at node_end
                            accumulated_length_end   =
                                accumulated_length[node_end[reach_current]]
                            accumulated_length_start = 
                                accumulated_length[node_end[reach_current]]+len[reach_current]
                        else
                            if ismissing(accumulated_length[node_end[reach_current]])
                                # current reach had been reached from node_start:
                                #     node_start: keep length
                                #     node_end:   add length of reach to length at node_start
                                accumulated_length_start =
                                    accumulated_length[node_start[reach_current]]
                                accumulated_length_end   = 
                                    accumulated_length[node_start[reach_current]]+len[reach_current]
                            else
                                # current reach had been reached from both ends:
                                #     node_start: use min of length at node_start and 
                                #                 length at node_end plus length of current reach
                                #     node_end  : use min of length at node_end and 
                                #                 length at node_start plus length of current reach
                                accumulated_length_start =
                                    min(accumulated_length[node_start[reach_current]],
                                        accumulated_length[node_end[reach_current]]+len[reach_current])
                                accumulated_length_end =
                                    min(accumulated_length[node_end[reach_current]],
                                        accumulated_length[node_start[reach_current]]+len[reach_current])
                            end
                        end
                        accumulated_length[node_start[reach_current]] = accumulated_length_start
                        accumulated_length[node_end[reach_current]]   = accumulated_length_end
                    end

                    # find adjacent reaches:
    
                    adj_reaches = Int[]
                    if accumulated_length[node_start[reach_current]] < thresh_length
                        if allowed_nodes[node_start[reach_current]]
                            adj_reaches = vcat(adj_reaches,
                                               findall(isequal(node_start[reach_current]),node_start),
                                               findall(isequal(node_start[reach_current]),node_end))
                        end
                    end
                    if accumulated_length[node_end[reach_current]] < thresh_length
                        if allowed_nodes[node_end[reach_current]]
                            adj_reaches = vcat(adj_reaches,
                                               findall(isequal(node_end[reach_current]),node_start),
                                               findall(isequal(node_end[reach_current]),node_end))
                        end
                    end

                    # println(string("current reach: ",reach_current,
                    #                ", region: ",region),
                    #                ", length: ",len[reach_current],
                    #                ", acc. lengths: ",accumulated_length[node_end[reach_current]],",",accumulated_length[node_start[reach_current]])
                    # println(string("adjacent reaches: ",adj_reaches))

                    # complement list of reaches to visit:

                    if length(adj_reaches) > 0
                        for ar in adj_reaches
                            if regions[ar] == 0; push!(to_visit,ar); end
                        end
                        to_visit = unique(to_visit)
                    end

                    # check for end of region and continue loop or break:
    
                    if length(to_visit) == 0; break; end
                    reach_current = to_visit[1]
                    deleteat!(to_visit,1)
     
                end
    
                # start new region in same subnet:
    
                ind = findfirst(isequal(0),regions[reaches_s])
                if isnothing(ind); break; end
                reach_current = reaches_s[ind]
    
            end

        end

    end
    for i = 1:n_reach; if ! allowed_reaches[i]; regions[i] = 0; end; end

    if verbose
        println(string("Rivernet_ConnectedRegions: ",maximum(regions),
                       " connected regions identified"))
    end

    return(regions)

end