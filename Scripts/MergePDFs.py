import os
from pypdf import PdfWriter

rivers = ["Zulg","Gluetschbach","Rotache","Chise","Giesse","Guerbe","Kander","Birs"]

for river in rivers:

    pdfs = [river + "_Plot_ConnectedRegions_cur_nons_Plots.pdf", river + "_Plot_ConnectedRegions_cur_salm_Plots.pdf"]

    i = 1
    while os.path.exists(river + "_Plot_ConnectedRegions_Opt" + str(i) + "_nons_Plots.pdf"):
        pdfs = pdfs + [river + "_Plot_ConnectedRegions_Opt" + str(i) + "_nons_Plots.pdf", river + "_Plot_ConnectedRegions_Opt" + str(i) + "_salm_Plots.pdf"]
        i = i+1

    writer = PdfWriter()

    for pdf in pdfs:
        writer.append(pdf)

    writer.write(river + "_Plot_ConnectedRegions_Opts_Plots.pdf")
    writer.close()

