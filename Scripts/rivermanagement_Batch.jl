## -------------------------------------------------------
##
## File: rivermanagement_Batch.jl
##
## December 21, 2024 -- Peter Reichert
## peter.reichert@emeriti.eawag.ch
##
## -------------------------------------------------------


# options:
# ========

# define input and output directories:

dir_input        = "../Input"
dir_output       = "../Output"
# dir_alternatives = "../Input"
dir_alternatives = "../Alternatives"

# alternatives extension:

# alt_ext = ["Alt"]
alt_exts = ["Opt","OptIso"]

# select river(s):

# rivers = ["Demo0a","Demo0b",","Demo1","Demo2","Demo3"]
# rivers = ["Demo0a","Demo0b","Demo0c"]
# rivers = ["Demo0a","Demo0b","Demo0c","Demo1","Demo2","Demo3",
#           "Kander","Birs",
#           "Zulg","Gluetschbach","Rotache","Chise","Giesse","Guerbe",
#           "Dreiwaesserkanal",
#           "Aa","Suhre","Toess","Broye"]
rivers = ["Example",
          "Zulg","Gluetschbach","Rotache","Chise","Giesse","Guerbe","Kander","Birs"]
# rivers = ["Birs"]
# rivers = ["Giesse"]
# rivers = ["Zulg","Giesse","Gluetschbach"]
# rivers = ["Rotache"]
# rivers = ["Guerbe"]
# rivers = ["Chise"]
# rivers = ["Zulg"]
# rivers = ["Kander"]
# rivers = ["Gluetschbach"]
# rivers = ["Example","Zulg"]
# rivers = ["Example"]

# define parameters:

crit_label      = ["nons","salm"]
crit_height_nat = [50.5,80.5]
crit_height_art = [20.5,50.5]
crit_slope      = [0.1,0.2]
thresh_length   = 30.0

dodisplay       = false

# define plot library:

rivermanagement_Plotlib = "Plots"
# rivermanagement_Plotlib = "Plotly"

# define plot options:

if rivermanagement_Plotlib == "Plots"
    size_node_barrier        = [2.0,3.0]   # same dimension as crit_height and crit_label
    size_node_barrier_rivers = Dict("Demo0a"           => [4.0,6.0],
                                    "Demo0b"           => [4.0,6.0],
                                    "Demo1"            => [4.0,6.0],
                                    "Demo2"            => [4.0,6.0],
                                    "Demo3"            => [4.0,6.0],
                                    "Example"          => [10.0,20.0],
                                    "Kander"           => [4.0,6.0],
                                    "Birs"             => [6.0,6.0],
                                    "Zulg"             => [3.0,4.5],
                                    "Gluetschbach"     => [6.0,9.0],
                                    "Rotache"          => [3.0,4.5],
                                    "Chise"            => [4.0,6.5],
                                    "Giesse"           => [6.0,9.0],
                                    "Guerbe"           => [6.0,9.0],
                                    "Dreiwaesserkanal" => [3.0,4.5],
                                    "Aa"               => [3.0,4.5],
                                    "Suhre"            => [4.0,6.0],
                                    "Toess"            => [2.0,3.0],
                                    "Broye"            => [3.0,4.5])
    lwd_reach                = 4.0
    lwd_reach_rivers         = Dict("Example"          => 8.0)
    lwd_culvert              = 8.0
    lwd_culvert_rivers       = Dict("Example"          => 16.0)
    lwd_missing              = 0.3
else
    size_node_barrier        = [4.0,6.0]   # same dimension as crit_height and crit_label
    # size_node_barrier        = [10,10]   # for identifying nodes when zooming
    size_node_barrier_rivers = Dict("Demo0a"           => [6.0,9.0],
                                    "Demo0b"           => [6.0,9.0],
                                    "Demo1"            => [6.0,9.0],
                                    "Demo2"            => [6.0,9.0],
                                    "Demo3"            => [6.0,9.0],
                                    "Example"          => [6.0,9.0],
                                    "Kander"           => [4.0,6.0],
                                    "Birs"             => [4.0,6.0],
                                    "Zulg"             => [4.0,6.0],
                                    "Gluetschbach"     => [6.0,9.0],
                                    "Rotache"          => [4.0,6.0],
                                    "Chise"            => [6.0,9.0],
                                    "Giesse"           => [6.0,9.0],
                                    "Guerbe"           => [6.0,9.0],
                                    "Dreiwaesserkanal" => [4.0,6.0],
                                    "Aa"               => [6.0,9.0],
                                    "Suhre"            => [6.0,9.0],
                                    "Toess"            => [4.0,6.0],
                                    "Broye"            => [6.0,9.0])
    lwd_reach                = 4.0
    lwd_reach_rivers         = Dict("Example"          => 8.0)
    lwd_culvert              = 6.0
    lwd_culvert_rivers       = Dict("Example"          => 12.0)
    lwd_missing              = 0.3
end

color_removed = "lime"
fact_removed  = 1.0

# define column names of attributes:

col_reach_covered = "EINDOL"      # 0: not covered; 1: covered
col_reach_width   = "GSBREITE"    # width of river bed
col_reach_order   = "FLOZ"        # stream order
col_node_type     = "BarrierType" # 0: no drop; 1: natural drop; 2: artificial drop or construction
col_node_height   = "Height"      # drop height in cm
col_reach_length  = "length"      # length of river reach
col_reach_slope   = "slope"       # mean slope of river reach
col_reach_morph   = "val_morph"   # morphological state [0,1]; good if >= 0.6
col_reach_imp     = "ecol_imp"    # ecological importance; default 1, factor > 1 for higher potential (< 1 for lower)



# import libraries:
# =================

import Pkg
Pkg.activate(".")
Pkg.instantiate()

include("rivermanagement.jl")


# plot colors:
# ============

cols = rivermanagement_GetRegionColors(20)
# rivermanagement_PlotColors(cols)


# loop over rivers:
# =================

for river in rivers

    for alt_ext in alt_exts

    println(string("processing river ",river," ..."))

    # default sizes:
    size_node_barrier_river = size_node_barrier
    lwd_reach_river         = lwd_reach
    lwd_culvert_river       = lwd_culvert
    # use specific sizes per river basin if available:
    if river in keys(size_node_barrier_rivers); size_node_barrier_river = size_node_barrier_rivers[river]; end
    if river in keys(lwd_reach_rivers);         lwd_reach_river         = lwd_reach_rivers[river]; end
    if river in keys(lwd_culvert_rivers);       lwd_culvert_river       = lwd_culvert_rivers[river]; end

    # read river network:
    # ===================

    # test reading of original files:
    # reach_coord  = CSV.read(string(dir_input,"/",river,"_Reach_Coord.csv"),  DataFrame; delim=';')
    # reach_attrib = CSV.read(string(dir_input,"/",river,"_Reach_Attrib_val_morph.csv"), DataFrame; delim=';')
    # node_attrib  = CSV.read(string(dir_input,"/",river,"_Node_Attrib.csv"),  DataFrame; delim=';')

    @time rivernet = rivermanagement_Read(string(dir_input,"/",river,"_Reach_Coord.csv"),
                                          string(dir_input,"/",river,"_Reach_Attrib_val_morph.csv"),
                                          string(dir_input,"/",river,"_Node_Attrib.csv"),
                                          sep     = ';')
    if ismissing(rivernet); error(string("*** problem reading data for river ",river)); end

    # replace missing reach width by mean width of the same reach order

    if any(ismissing.(rivernet["attrib_reach"][!,col_reach_width]))
        max_order = maximum(skipmissing(rivernet["attrib_reach"][!,col_reach_order]))
        width_order = Vector{Float64}(undef,max_order)
        for order in 1:max_order; width_order[order] = mean(skipmissing(rivernet["attrib_reach"][findall(isequal(order),rivernet["attrib_reach"][!,col_reach_order]),col_reach_width])); end
        println("order width"); println(hcat(1:max_order,width_order))
        for i in 1:nrow(rivernet["attrib_reach"])
            if ismissing(rivernet["attrib_reach"][i,col_reach_width])
                if !ismissing(rivernet["attrib_reach"][i,col_reach_order])
                    rivernet["attrib_reach"][i,col_reach_width] = width_order[rivernet["attrib_reach"][i,col_reach_order]]
                end
            end
        end
    end

    # write river network attributes:
    # ===============================

    rivermanagement_Write(rivernet,
                          string(dir_output,"/",river,"_Reach_Attrib.dat"),
                          string(dir_output,"/",river,"_Node_Attrib.dat");
                          sep = '\t')

    regions = DataFrame(Reach_ID=rivernet["attrib_reach"][!,"Reach_ID"])

    # derive properties:
    # ==================

    n_node  = nrow(rivernet["attrib_node"])
    n_reach = nrow(rivernet["attrib_reach"])

    nodetype = rivernet["attrib_node"][:,col_node_type]  # 0: no drop; 1: natural drop; 2: artificial drop
    replace!(nodetype,missing=>0)                        # replace missing information by no drop
    nodeheight = rivernet["attrib_node"][:,col_node_height]  # drop height in cm
    replace!(nodeheight,missing=>0.0)                    # replace missing information by zero height
    # nodeheight_class: 0: below any crit. height, i: above crit. height i, below crit. height i+1
    nodeheight_class = zeros(Int,n_node)
    for i in 1:n_node
        if nodetype[i] == 1
            for j in 1:length(crit_label); if nodeheight[i] >= crit_height_nat[j]; nodeheight_class[i] = j; end; end
        end
        if nodetype[i] == 2
            for j in 1:length(crit_label); if nodeheight[i] >= crit_height_art[j]; nodeheight_class[i] = j; end; end
        end
    end

    covered = rivernet["attrib_reach"][:,col_reach_covered]
    replace!(covered,missing=>0)                         # replace missing information by not covered
    replace!(covered,9=>0)                               # replace 9 by not covered ??
    culvert     = [false,true][covered.+1]
    replace!(culvert,missing=>false)
    val_morph   = rivernet["attrib_reach"][:,col_reach_morph]
    val_morph_missing1 = copy(val_morph)
    replace!(val_morph_missing1,missing=>1.0)
    if col_reach_imp in names(rivernet["attrib_reach"])
        val_imp = rivernet["attrib_reach"][:,col_reach_imp]
        replace!(val_imp,missing=>1.0)
    else
        val_imp = fill(1.0,n_reach)
    end


    # plot subnets:
    # =============

    p1 = rivermanagement_Plot(rivernet,
                              title     = string(river,": subnets (",rivernet["n_subnet"],")"),
                              col_reach = getindex(["black","blue","green","red","orange","purple","cyan","violet"],mod.(rivernet["attrib_reach"].subnet,8).+1),
                              lwd_reach = lwd_reach_river,
                              dodisplay = dodisplay,
                              filename  = string(dir_output,"/",river,"_Plot_Subnets_",rivermanagement_Plotlib,".pdf"))


    # plot stream order:
    # ==================

    streamorder = rivernet["attrib_reach"][:,col_reach_order]
    replace!(streamorder,missing=>0)
    p2 = rivermanagement_Plot(rivernet,
                              title     = string(river,": stream order"),
                              col_reach = ["black","blue","green","red","orange","purple","cyan","violet"][streamorder.+1],
                              lwd_reach = lwd_missing .+ 0.5.*lwd_reach_river*(streamorder),
                              dodisplay = dodisplay,
                              filename  = string(dir_output,"/",river,"_Plot_StreamOrder_",rivermanagement_Plotlib,".pdf"))


    # plot morphological state:
    # =========================

    ind_culvert = findall(culvert)
    ind_missing = findall(ismissing.(val_morph))
    col_morph = rivermanagement_GetMSKColor.(val_morph)
    col_morph[ind_culvert] .= "black"
    col_morph[ind_missing] .= "black"
    lwd_morph = fill(lwd_reach_river,n_reach)
    lwd_morph[ind_culvert] .= lwd_culvert_river
    lwd_morph[ind_missing] .= lwd_missing
    p3 = rivermanagement_Plot(rivernet,
                              title     = string(river,": morphological state"),
                              col_reach = col_morph,
                              lwd_reach = lwd_morph,
                              col_node  = ["black","blue","red"][nodetype .+ 1],
                              size_node = vcat(0.0,size_node_barrier_river)[nodeheight_class.+1],
                              dodisplay = dodisplay,
                              filename  = string(dir_output,"/",river,"_Plot_MorphState_",rivermanagement_Plotlib,".pdf"))
    

    # plot fishecological importance:
    # ===============================

    col_ecolimp = fill("black",n_reach)
    f_imp = 1.414
    if river == "Kander"; f_imp = 1.260; end
    tol   = 0.02
    for i in 1:n_reach
        if val_imp[i] < 1.0 - tol
            if val_imp[i] > 1.0/f_imp - tol
                col_ecolimp[i] = "coral"
            elseif val_imp[i] > 1.0/f_imp^2 - tol
                col_ecolimp[i] = "red"
            else
                col_ecolimp[i] = "red3"
            end
        elseif val_imp[i] > 1.0 + tol
            if val_imp[i] < f_imp + tol
                col_ecolimp[i] = "palegreen"
            elseif val_imp[i] < f_imp^2 + tol
                col_ecolimp[i] = "springgreen3"
            else
                col_ecolimp[i] = "darkgreen"
            end
        end
    end
    p3 = rivermanagement_Plot(rivernet,
                              title     = string(river,": fishecological importance"),
                              col_reach = col_ecolimp,
                              lwd_reach = lwd_reach_river,
                              dodisplay = dodisplay,
                              filename  = string(dir_output,"/",river,"_Plot_EcolImp_",rivermanagement_Plotlib,".pdf"))
    

    # prepare input for calculation of attributes and plotting:
    # =========================================================

    barrier_nat = Vector{Vector{Bool}}(undef,length(crit_label))
    barrier_art = Vector{Vector{Bool}}(undef,length(crit_label))
    attrib_cur  = Array{Any}(undef,length(crit_label))
    attrib_nat  = Array{Any}(undef,length(crit_label))
    toosteep    = Vector{Vector{Bool}}(undef,length(crit_label))
    for i in 1:length(crit_label)
        barrier_nat[i] = ([false,true,false][nodetype.+1]) .&& (nodeheight .> crit_height_nat[i] )
        barrier_art[i] = ([false,false,true][nodetype.+1]) .&& (nodeheight .> crit_height_art[i] ) 
        slope = copy(rivernet["attrib_reach"][:,col_reach_slope])
        replace!(slope,missing=>0.0)
        toosteep[i] = slope .> crit_slope[i]
    end

    # for testing only
    # regions_nat = rivermanagement_ConnectedRegions(rivernet;
    #                                                crit_reach    = .! toosteep[1],
    #                                                crit_node     = ([true,false,true][nodetype.+1]) .| (nodeheight .<= crit_height_nat[1] ),
    #                                                thresh_length = thresh_length)              
    # regions_inv = rivermanagement_ConnectedRegions(rivernet;
    #                                                crit_reach    = ( (.! culvert) .& (.! toosteep[1]) ),
    #                                                crit_node     = ([true,false,false][nodetype.+1]) .| (nodeheight .<= crit_height_art[1] ),
    #                                                thresh_length = thresh_length)              
    # end of testing


    # get and plot connected regions for natural and current states:
    # ==============================================================

    colnames_add = vcat(string.("n_reg_",crit_label),["barrier_area","culvert_length","rehab_costs"])
    attgroup_ext = ["","_goodmorph","_noimp","_goodmorphnoimp"]
    attgroup_names = vcat("att",string.("att",attgroup_ext[2:end]))

    attrib_names    = String[]
    colnames_attrib = String[]
    attrib_all      = missing

    for i in 1:length(crit_label)

        # calculate attributes:

        attrib_cur[i] = rivermanagement_CalcAttributes(
                            rivernet,                          # rivernet
                            barrier_nat[i],                    # barrier_nat  # which nodes are natual barriers?
                            barrier_art[i],                    # barrier_art  # which nodes are artificial barriers?
                            convert(Vector{Bool},toosteep[i]), # badreach_nat # which reaches are inappropriate naturally? (e.g. too steep for fish)
                            convert(Vector{Bool},culvert),     # badreach_art # which reaches are inappropriate artificially? (e.g. culverts)
                            thresh_length = thresh_length,
                            col_length    = col_reach_length,
                            col_order     = col_reach_order,
                            reach_morph   = val_morph_missing1,
                            reach_imp     = val_imp)

        regions[!,string(crit_label[i],"_","nat")] = attrib_cur[i]["regions_nat"]
        regions[!,string(crit_label[i],"_","cur")] = attrib_cur[i]["regions_inv"]
                
        # calculate attributes for natural river network:

        attrib_nat[i] = rivermanagement_CalcAttributes(
                            rivernet,                          # rivernet
                            barrier_nat[i],                    # barrier_nat  # which nodes are natual barriers?
                            fill(false,n_node),                # barrier_art  # which nodes are artificial barriers?
                            convert(Vector{Bool},toosteep[i]), # badreach_nat # which reaches are inappropriate naturally? (e.g. too steep for fish)
                            fill(false,n_reach);               # badreach_art # which reaches are inappropriate artificially? (e.g. culverts)
                            thresh_length = thresh_length,
                            col_length    = col_reach_length,
                            col_order     = col_reach_order,
                            reach_morph   = 1.0,
                            reach_imp     = val_imp)

        # collect attributes in DataFrame:

        if i == 1
            colnames_attrib = ["alternative"]
            attrib_names = string.(keys(attrib_cur[i]["att"]))
            attrib_names_ext = [string("$a","$b") for b in vcat("",attgroup_ext[2:end]) for a in attrib_names]
            for lab in crit_label; colnames_attrib = vcat(colnames_attrib,string.(attrib_names_ext,"_",lab)); end
            colnames_attrib = vcat(colnames_attrib,colnames_add)
            attrib_all = DataFrame([name => [] for name in colnames_attrib])
            push!(attrib_all,hcat(["nat"],transpose(fill(missing,ncol(attrib_all)-1))))
            push!(attrib_all,hcat(["cur"],transpose(fill(missing,ncol(attrib_all)-1))))
            attrib_all[(end-1):end,(end-length(colnames_add)+1):end] .= 0.0     # set alternative characterizing attributes to zero
        end

        for att in attrib_names
            for j in 1:length(attgroup_ext)
                attrib_all[end-1,string(att,attgroup_ext[j],"_",crit_label[i])] = attrib_nat[i][attgroup_names[j]][att]
                attrib_all[end-1,string("n_reg_",crit_label[i])] = attrib_nat[i]["n_inv"]
                attrib_all[end  ,string(att,attgroup_ext[j],"_",crit_label[i])] = attrib_cur[i][attgroup_names[j]][att]
                attrib_all[end  ,string("n_reg_",crit_label[i])] = attrib_cur[i]["n_inv"]
            end
        end

        digits = 2

        col_region = rivermanagement_GetRegionColors(attrib_cur[i]["n_inv"]+1)
        lwd        = fill(lwd_reach_river,n_reach)
        lwd[findall(culvert)]     .= lwd_culvert_river
        lwd[findall(toosteep[i])] .= lwd_missing
        global p6 = rivermanagement_Plot(
                        rivernet,
                        title     = string(river,", cur, ",crit_label[i],": ",
                                           "a_ups=",round(attrib_cur[i]["att"]["a_ups"],digits=digits)," ",
                                           "a_int=",round(attrib_cur[i]["att"]["a_int"],digits=digits)," ",
                                           " (",attrib_cur[i]["n_inv"]," reg)"),
                        col_reach = col_region[attrib_cur[i]["regions_inv"].+1],
                        lwd_reach = lwd,
                        col_node  = ["black","blue","red"][nodetype .+ 1],
                        size_node = vcat(0.0,size_node_barrier_river)[nodeheight_class.+1],
                        dodisplay = dodisplay,
                        filename  = string(dir_output,"/",river,"_Plot_ConnectedRegions_cur_",crit_label[i],"_",rivermanagement_Plotlib,".pdf"))
                                             
        col_region = rivermanagement_GetRegionColors(attrib_cur[i]["n_nat"]+1)
        lwd        = fill(lwd_reach_river,n_reach)
        lwd[findall(toosteep[i])] .= lwd_missing
        size       = vcat(0.0,size_node_barrier_river)[nodeheight_class.+1]
        size[findall(nodetype.==0)] .= 0.0
        size[findall(nodetype.==2)] .= 0.0
        global p7 = rivermanagement_Plot(rivernet,
                        title     = string(river,", nat, ",crit_label[i],": ",
                                           "a_ups=",round(attrib_nat[i]["att"]["a_ups"],digits=digits)," ",
                                           "a_int=",round(attrib_nat[i]["att"]["a_int"],digits=digits)," ",
                                           " (",attrib_nat[i]["n_inv"]," reg)"),
                        col_reach = col_region[attrib_nat[i]["regions_nat"].+1],
                        lwd_reach = lwd,
                        col_node  = ["black","blue"][2 .- abs.(nodetype .- 1)],
                        size_node = size,
                        dodisplay = dodisplay,
                        filename  = string(dir_output,"/",river,"_Plot_ConnectedRegions_nat_",crit_label[i],"_",rivermanagement_Plotlib,".pdf"))
    
    end

    # get and plot attributes and connected regions for rehabilitation alternatives:
    # ------------------------------------------------------------------------------

    files = readdir(dir_alternatives)
    corefilename = string(river,"_",alt_ext)
    ind = findall(isequal(corefilename),SubString.(files,1,min.(length(corefilename),length.(files))))
    if length(ind) > 0

        alt = 1

        while isfile(string(dir_alternatives,"/",river,"_",alt_ext,alt,"_barriers.csv")) | isfile(string(dir_alternatives,"/",river,"_",alt_ext,alt,"_culverts.csv"))

            push!(attrib_all,hcat([string("_",alt_ext,alt)],transpose(fill(missing,ncol(attrib_all)-1))))

            barriers_toremove = Int[]
            culverts_toremove = Int[]
            if isfile(string(dir_alternatives,"/",river,"_",alt_ext,alt,"_barriers.csv"))
                tmp = CSV.read(string(dir_alternatives,"/",river,"_",alt_ext,alt,"_barriers.csv"),DataFrame,delim=";")
                barriers_toremove = tmp[:,1]
            end
            if isfile(string(dir_alternatives,"/",river,"_",alt_ext,alt,"_culverts.csv"))
                tmp = CSV.read(string(dir_alternatives,"/",river,"_",alt_ext,alt,"_culverts.csv"),DataFrame,delim=";")
                culverts_toremove = tmp[:,1]
            end

            area,lengthorder,costs = rivermanagement_GetRehabCosts(rivernet,barriers_toremove,culverts_toremove,
                                                                   col_node_height,col_reach_width,col_reach_length,col_reach_order)
            attrib_all[end,(end-2):end] = [area,lengthorder,costs]

            barrier_art_alt = Vector{Vector{Bool}}(undef,length(crit_label))
            culvert_alt     = copy(culvert);
            attrib_alt      = Array{Any}(undef,length(crit_label))
            if length(culverts_toremove) > 0; culvert_alt[culverts_toremove] .= false; end
            for i in 1:length(crit_label)

                barrier_art_alt[i] = copy(barrier_art[i])
                if length(barriers_toremove) > 0; barrier_art_alt[i][barriers_toremove] .= false; end

                attrib_alt[i] = rivermanagement_CalcAttributes(
                                    rivernet,                          # rivernet
                                    barrier_nat[i],                    # barrier_nat  # which nodes are natual barriers?
                                    barrier_art_alt[i],                # barrier_art  # which nodes are artificial barriers?
                                    convert(Vector{Bool},toosteep[i]), # badreach_nat # which reaches are inappropriate naturally? (e.g. too steep for fish)
                                    convert(Vector{Bool},culvert_alt), # badreach_art # which reaches are inappropriate artificially? (e.g. culverts)
                                    thresh_length = thresh_length,
                                    col_length    = col_reach_length,
                                    col_order     = col_reach_order,
                                    reach_morph   = val_morph_missing1,
                                    reach_imp     = val_imp)

                regions[!,string(crit_label[i],"_",alt_ext,alt)] = attrib_alt[i]["regions_inv"]

                for att in attrib_names
                    for j in 1:length(attgroup_ext)
                        attrib_all[end,string(att,attgroup_ext[j],"_",crit_label[i])] = attrib_alt[i][attgroup_names[j]][att]
                        attrib_all[end  ,string("n_reg_",crit_label[i])] = attrib_alt[i]["n_inv"]
                    end
                end
        
                digits = 2
    
                col_region = rivermanagement_GetRegionColors(attrib_alt[i]["n_inv"]+1)
                col_reach_alt = col_region[attrib_alt[i]["regions_inv"].+1]
                if length(culverts_toremove) > 0; col_reach_alt[culverts_toremove] .= color_removed; end
                lwd        = fill(lwd_reach_river,n_reach)
                lwd[findall(culvert)]     .= lwd_culvert_river
                lwd[findall(toosteep[i])] .= lwd_missing
                col_node_alt = ["black","blue","red"][nodetype .+ 1]
                size_node_alt = vcat(0.0,size_node_barrier_river)[nodeheight_class.+1]
                if length(barriers_toremove) > 0
                    col_node_alt[barriers_toremove] .= color_removed
                    size_node_alt[barriers_toremove] .= fact_removed .* size_node_alt[barriers_toremove]
                end
                len_culvertsremoved = 0.0
                if length(culverts_toremove) > 0
                    len_culvertsremoved = sum(rivernet["attrib_reach"][culverts_toremove,col_reach_length])
                end
                global p8 = rivermanagement_Plot(
                                rivernet,
                                title     = string(river,", alt",alt,
                                                   "(",length(barriers_toremove),
                                                   ",",round(Int,len_culvertsremoved),"m",
                                                   if !ismissing(costs); string(",",round(costs,digits=1),"MFr"); else ""; end,
                                                   ") ",
                                                   crit_label[i],": ",
                                                   "a_ups=",round(attrib_alt[i]["att"]["a_ups"],digits=digits)," ",
                                                   "a_int=",round(attrib_alt[i]["att"]["a_int"],digits=digits)," ",
                                                   " (",attrib_alt[i]["n_inv"]," reg)"),
                                        col_reach = col_reach_alt,
                                lwd_reach = lwd,
                                col_node  = col_node_alt,
                                size_node = size_node_alt,
                                dodisplay = dodisplay,
                                filename  = string(dir_output,"/",river,"_Plot_ConnectedRegions","_",alt_ext,alt,"_",crit_label[i],"_",rivermanagement_Plotlib,".pdf"))
                                                              
            end
    
            alt = alt + 1

        end

    end

    CSV.write(string(dir_output,"/",river,"_Regions.dat"),regions,delim="\t")
    CSV.write(string(dir_output,"/",river,"_attrib.csv"),attrib_all;delim=";")

end # for alt_ext in ...
end # for river in ...
