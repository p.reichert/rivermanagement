## -------------------------------------------------------
##
## File: rivermanagement_UpstreamConnectivity.jl
##
## August 14, 2022 -- Peter Reichert
## peter.reichert@emeriti.eawag.ch
##
## -------------------------------------------------------


function rivermanagement_UpstreamConnectivity(net;
                                              crit_reach    = true,
                                              crit_node     = true,
                                              thresh_length = 0)

    # check arguments:
    # ================

    if ! ( haskey(net,"attrib_reach") & haskey(net,"attrib_node") & haskey(net,"paths") )
        println(string("rivermanagement_UpstreamConnectivity: *** argument \"net\" ",
                       "needs keys \"attrib_reach\", \"attrib_node\" and \"paths\" ***"))
        return
    end
    if ! ( ( columnindex(net["attrib_reach"],"length") > 0 ) &
           ( columnindex(net["attrib_reach"],"from_node") > 0 ) &
           ( columnindex(net["attrib_reach"],"to_node") > 0 ) )
           println(string("rivermanagement_UpstreamConnectivity: *** element \"attrib_reach\" ",
                          "of argument \"net\" needs keys \"length\", \"from_node\" ",
                          "and \"to_node\" ***"))
        return
    end
   
    # initialize variables:
    # =====================

    n_reach    = nrow(net["attrib_reach"])
    n_node     = nrow(net["attrib_node"])

    paths      = net["paths"]
    lengths    = net["attrib_reach"].length
    from_nodes = net["attrib_reach"].from_node
    to_nodes   = net["attrib_reach"].to_node

    if (! isa(crit_node,Array)) && (! isa(crit_node,BitVector))
        allowed_nodes = fill(crit_node,n_node)
    elseif length(crit_node) == 1
        allowed_nodes = fill(crit_node[1],n_node)
    elseif length(crit_node) == n_node
        allowed_nodes = crit_node
    else
        println(string("rivermanagement_UpstreamConnectivity: *** argument \"crit_node\" ",
                       "has to be a bool scalar or vector of length 1 or number of nodes"))
    end
    if (! isa(crit_reach,Array)) && (! isa(crit_reach,BitVector))
        allowed_reaches = fill(crit_reach,n_reach)
    elseif length(crit_reach) == 1
        allowed_reaches = fill(crit_reach[1],n_reach)
    elseif length(crit_reach) == n_reach
        allowed_reaches = crit_reach
    else
        println(string("rivermanagement_UpstreamConnectivity: *** argument \"crit_reach\" ",
                       "has to be a bool scalar or vector of length 1 or number of reaches"))
    end

    # find reachable reaches:
    # =======================
 
    reaches_reachable = zeros(Int,n_reach)
    for i in 1:length(paths)
        length_offset = 0
        for j in 1:length(paths[i])
            reach = paths[i][length(paths[i])+1-j]
            if ! allowed_nodes[to_nodes[reach]] # reach cannot be reached
                break
            end
            if allowed_reaches[reach]           # reach is reachable and allowed
                reaches_reachable[reach] = 1    # indicate reach reachable
                length_offset = 0               # reset length of not allowed reaches
            else                                # reach is not allowed
                if length_offset + lengths[reach] > thresh_length
                    break                       # stop, if not allowed length to large
                else                            # otherwise increase length of not allowed reaches
                    length_offset = length_offset + lengths[reach]
                end
           end
        end
    end

    println(string("rivermanagement_UpstreamConnectivity: ",sum(reaches_reachable),
                   " reachable reaches identified"))

    return reaches_reachable

end

