## -------------------------------------------------------
##
## File: rivermanagement_Optimize.jl
##
## December 26, 2024 -- Peter Reichert
## peter.reichert@emeriti.eawag.ch
##
## -------------------------------------------------------


# import libraries:
# =================

import Pkg
Pkg.activate(".")
Pkg.instantiate()

rivermanagement_Plotlib = "Plots"
include("rivermanagement.jl")

import Combinatorics


# options:
# ========

# define input and output directories:

dir_input        = "../Input"
dir_output       = "../Output"
# dir_alternatives = "../Input"
dir_alternatives = "../Alternatives"

# alternatives extension:

# alt_ext_loss = "Alt"
# alt_ext_gain = "AltIso"
alt_ext_loss = "Opt"
alt_ext_gain = "OptIso"

# select river(s):

# rivers = ["Demo0a","Demo0b",","Demo1","Demo2","Demo3"]
# rivers = ["Demo0a","Demo0b","Demo0c"]
# rivers = ["Demo0a","Demo0b","Demo0c","Demo1","Demo2","Demo3",
#           "Kander","Birs",
#           "Zulg","Gluetschbach","Rotache","Chise","Giesse","Guerbe",
#           "Dreiwaesserkanal",
#           "Aa","Suhre","Toess","Broye"]
rivers = ["Example",
          "Zulg","Gluetschbach","Rotache","Chise","Giesse","Guerbe","Kander","Birs"]
# rivers = ["Birs"]
# rivers = ["Giesse"]
# rivers = ["Zulg","Giesse","Gluetschbach"]
# rivers = ["Rotache"]
# rivers = ["Guerbe"]
# rivers = ["Chise"]
# rivers = ["Zulg"]
# rivers = ["Kander"]
# rivers = ["Gluetschbach"]
# rivers = ["Example","Zulg"]
# rivers = ["Example"]

# select optimization alternatives:

Alternatives = (Zulg         = [11,12,13,22,47,69],
                Gluetschbach = [16],
                Rotache      = [10,69],
                Chise        = [41],
                Giesse       = [19],
                Guerbe       = [100],
                Kander       = [23,58],
                Birs         = [9,12])

# define parameters:

crit_label      = ["nons","salm"]
crit_height_nat = [50.5,80.5]
crit_height_art = [20.5,50.5]
crit_slope      = [0.1,0.2]
thresh_length   = 30.0

n_opt           = 6
delta_value     = 0.01

# define column names of attributes:

col_reach_covered = "EINDOL"      # 0: not covered; 1: covered
col_reach_width   = "GSBREITE"    # width of river bed
col_reach_order   = "FLOZ"        # stream order
col_node_type     = "BarrierType" # 0: no drop; 1: natural drop; 2: artificial drop or construction
col_node_height   = "Height"      # drop height in cm
col_reach_length  = "length"      # length of river reach
col_reach_slope   = "slope"       # mean slope of river reach
col_reach_morph   = "val_morph"   # morphological state [0,1]; good if >= 0.6
col_reach_imp     = "ecol_imp"    # ecological importance; default 1, factor > 1 for higher potential (< 1 for lower)


# function to evaluate value
# ==========================

function rivermanagement_CalcValue(rivernet,barrier_nat,barrier_art,toosteep,culvert,
                                   thresh_length,col_reach_length,col_reach_order,val_morph,val_imp)

    attrib = Array{Float64}(undef,length(crit_label),2)
    for i in 1:length(crit_label)
        res = rivermanagement_CalcAttributes(
                            rivernet,                          # rivernet
                            barrier_nat[i],                    # barrier_nat  # which nodes are natual barriers?
                            barrier_art[i],                    # barrier_art  # which nodes are artificial barriers for the current alternative?
                            convert(Vector{Bool},toosteep[i]), # badreach_nat # which reaches are inappropriate naturally? (e.g. too steep for fish)
                            culvert,                           # badreach_art # which reaches are inappropriate artificially for the current alternative (e.g. culverts)
                            thresh_length = thresh_length,
                            col_length    = col_reach_length,
                            col_order     = col_reach_order,
                            reach_morph   = val_morph,
                            reach_imp     = val_imp,
                            verbose       = false)
        attrib[i,:] .= [res["att"]["a_ups"],res["att"]["a_int"]]
    end
    value = Statistics.mean(attrib)
    
    return value,attrib

end


# loop over rivers:
# =================

for river in rivers

    # read river network:
    # ===================

    println(string("\nprocessing river ",river," ...\n"))

    # test reading of original files:
    # reach_coord  = CSV.read(string(dir_input,"/",river,"_Reach_Coord.csv"),  DataFrame; delim=';')
    # reach_attrib = CSV.read(string(dir_input,"/",river,"_Reach_Attrib_val_morph.csv"), DataFrame; delim=';')
    # node_attrib  = CSV.read(string(dir_input,"/",river,"_Node_Attrib.csv"),  DataFrame; delim=';')

    @time rivernet = rivermanagement_Read(string(dir_input,"/",river,"_Reach_Coord.csv"),
                                          string(dir_input,"/",river,"_Reach_Attrib_val_morph.csv"),
                                          string(dir_input,"/",river,"_Node_Attrib.csv"),
                                          sep     = ';')
    if ismissing(rivernet); error(string("*** problem reading data for river ",river)); end

    # replace missing reach width by mean width of the same reach order

    if any(ismissing.(rivernet["attrib_reach"][!,col_reach_width]))
        max_order = maximum(skipmissing(rivernet["attrib_reach"][!,col_reach_order]))
        width_order = Vector{Float64}(undef,max_order)
        for order in 1:max_order; width_order[order] = mean(skipmissing(rivernet["attrib_reach"][findall(isequal(order),rivernet["attrib_reach"][!,col_reach_order]),col_reach_width])); end
        println("order width"); println(hcat(1:max_order,width_order))
        for i in 1:nrow(rivernet["attrib_reach"])
            if ismissing(rivernet["attrib_reach"][i,col_reach_width])
                if !ismissing(rivernet["attrib_reach"][i,col_reach_order])
                    rivernet["attrib_reach"][i,col_reach_width] = width_order[rivernet["attrib_reach"][i,col_reach_order]]
                end
            end
        end
    end


    # write river network attributes:
    # ===============================

    rivermanagement_Write(rivernet,
                          string(dir_output,"/",river,"_Reach_Attrib.dat"),
                          string(dir_output,"/",river,"_Node_Attrib.dat");
                          sep = '\t')


    # derive properties:
    # ==================

    n_node  = nrow(rivernet["attrib_node"])
    n_reach = nrow(rivernet["attrib_reach"])

    nodetype = rivernet["attrib_node"][:,col_node_type]  # 0: no drop; 1: natural drop; 2: artificial drop
    replace!(nodetype,missing=>0)                        # replace missing information by no drop
    nodeheight = rivernet["attrib_node"][:,col_node_height]  # drop height in cm
    replace!(nodeheight,missing=>0.0)                    # replace missing information by zero height

    covered = rivernet["attrib_reach"][:,col_reach_covered]
    replace!(covered,missing=>0)                         # replace missing information by not covered
    replace!(covered,9=>0)                               # replace 9 by not covered ??
    culvert     = [false,true][covered.+1]
    replace!(culvert,missing=>false)
    val_morph   = rivernet["attrib_reach"][:,col_reach_morph]
    val_morph_missing1 = copy(val_morph)
    replace!(val_morph_missing1,missing=>1.0)            # replace missing morphology by good state (1)
    if col_reach_imp in names(rivernet["attrib_reach"])
        val_imp = rivernet["attrib_reach"][:,col_reach_imp]
        replace!(val_imp,missing=>1.0)
    else
        val_imp = fill(1.0,n_reach)
    end

    barrier_nat = Vector{Vector{Bool}}(undef,length(crit_label))
    barrier_art = Vector{Vector{Bool}}(undef,length(crit_label))
    attrib_cur  = Array{Any}(undef,length(crit_label))
    attrib_nat  = Array{Any}(undef,length(crit_label))
    toosteep    = Vector{Vector{Bool}}(undef,length(crit_label))
    for i in 1:length(crit_label)
        barrier_nat[i] = ([false,true,false][nodetype.+1]) .&& (nodeheight .> crit_height_nat[i] )
        barrier_art[i] = ([false,false,true][nodetype.+1]) .&& (nodeheight .> crit_height_art[i] ) 
        slope = copy(rivernet["attrib_reach"][:,col_reach_slope])
        replace!(slope,missing=>0.0)
        toosteep[i] = slope .> crit_slope[i]
    end


    # calculate removal costs and value loss for each obstacle
    # ========================================================

    # set-up variables
    barrier_art_ind = Int[]
    for i in 1:length(crit_label); barrier_art_ind = unique(vcat(barrier_art_ind,findall(barrier_art[i]))); end
    culvert_ind     = findall(culvert)
    n_barriers_art  = length(barrier_art_ind)
    n_culverts      = length(culvert_ind)

    # reference value with all artificial barriers and culverts and with other reaches in their current state
    barriers_art_tmp  = deepcopy(barrier_art)
    culverts_tmp      = copy(culvert)
    value_allart,attrib_allart = rivermanagement_CalcValue(rivernet,barrier_nat,barriers_art_tmp,toosteep,culverts_tmp,
                                                           thresh_length,col_reach_length,col_reach_order,val_morph_missing1,val_imp)

    # reference value without artificial barriers and culverts but with other reaches in their current state
    for i in 1:length(crit_label); barriers_art_tmp[i] .= false; end
    culverts_tmp .= false
    value_noart,attrib_noart   = rivermanagement_CalcValue(rivernet,barrier_nat,barriers_art_tmp,toosteep,culverts_tmp,
                                                           thresh_length,col_reach_length,col_reach_order,val_morph_missing1,val_imp)

    # containers to collect value loss (for barrier placements) and value gain (for barrier removals) data
    valueloss = DataFrame(Type       = fill("",n_barriers_art+n_culverts),
                          Index      = zeros(Int,n_barriers_art+n_culverts),
                          Culvert    = fill("",n_barriers_art+n_culverts),
                          Length     = zeros(n_barriers_art+n_culverts),
                          ValueLoss  = Vector{Union{Float64,Missing}}(missing,n_barriers_art+n_culverts),
                          Costs      = Vector{Union{Float64,Missing}}(missing,n_barriers_art+n_culverts),
                          SumCosts   = zeros(n_barriers_art+n_culverts),
                          Value      = zeros(n_barriers_art+n_culverts),
                          a_ups_nons = zeros(n_barriers_art+n_culverts),
                          a_int_nons = zeros(n_barriers_art+n_culverts),
                          a_ups_salm = zeros(n_barriers_art+n_culverts),
                          a_int_salm = zeros(n_barriers_art+n_culverts))
    valuegain = DataFrame(Type       = fill("",n_barriers_art+n_culverts),
                          Index      = zeros(Int,n_barriers_art+n_culverts),
                          Culvert    = fill("",n_barriers_art+n_culverts),
                          Length     = zeros(n_barriers_art+n_culverts),
                          ValueGain  = Vector{Union{Float64,Missing}}(missing,n_barriers_art+n_culverts),
                          Costs      = Vector{Union{Float64,Missing}}(missing,n_barriers_art+n_culverts),
                          SumCosts   = zeros(n_barriers_art+n_culverts),
                          Value      = zeros(n_barriers_art+n_culverts),
                          a_ups_nons = zeros(n_barriers_art+n_culverts),
                          a_int_nons = zeros(n_barriers_art+n_culverts),
                          a_ups_salm = zeros(n_barriers_art+n_culverts),
                          a_int_salm = zeros(n_barriers_art+n_culverts))

    culverts_tmp .= false
    for i in 1:n_barriers_art

        # calculate costs
        area,culvertlength,costs = rivermanagement_GetRehabCosts(rivernet,[barrier_art_ind[i]],Int[],
                                                                 col_node_height,col_reach_width,col_reach_length,col_reach_order)

        # calculate value loss
        for j in 1:length(crit_label)
            barriers_art_tmp[j] .= false    # temporarily build barrier i for appropriate fish group
            if barrier_art[j][barrier_art_ind[i]]; barriers_art_tmp[j][barrier_art_ind[i]] = true; end
        end
        value_l,attrib_l = rivermanagement_CalcValue(rivernet,barrier_nat,barriers_art_tmp,toosteep,culverts_tmp,
                                                     thresh_length,col_reach_length,col_reach_order,val_morph_missing1,val_imp)
        valueloss[i,1:6] = ["barrier",barrier_art_ind[i],"",0.0,value_noart-value_l,costs]

        # calculate value gain
        for j in 1:length(crit_label)
            barriers_art_tmp[j] .= barrier_art[j]
            barriers_art_tmp[j][barrier_art_ind[i]] = false      # temporarily remove barrier i for all fish groups
        end
        value_g,attrib_g = rivermanagement_CalcValue(rivernet,barrier_nat,barriers_art_tmp,toosteep,culvert,
                                                     thresh_length,col_reach_length,col_reach_order,val_morph_missing1,val_imp)
        valuegain[i,1:6] = ["barrier",barrier_art_ind[i],"",0.0,value_g-value_allart,costs]

    end

    for j in 1:length(crit_label); barriers_art_tmp[j] .= false; end
    for i in 1:n_culverts

        if ismissing(valueloss[n_barriers_art+i,"ValueLoss"])

            # collect connected sections
            culvert_sections = [culvert_ind[i]]
            n = 1
            while true
                nodes = Int[]
                for s in culvert_sections
                    push!(nodes,rivernet["attrib_reach"][s,"node_start"])
                    push!(nodes,rivernet["attrib_reach"][s,"node_end"])
                end
                nodes = unique(nodes)
                s = Int[]
                for n in nodes 
                    s = vcat(s,findall(isequal(n),rivernet["attrib_reach"][!,"node_start"]),
                               findall(isequal(n),rivernet["attrib_reach"][!,"node_end"]))
                end
                for j in 1:length(s)
                    if s[j] in culvert_ind; push!(culvert_sections,s[j]); end
                end
                culvert_sections = unique(culvert_sections)
                if length(culvert_sections) == n; break; end
                n = length(culvert_sections)
            end

            # calculate value loss
            culverts_tmp                   .= false
            culverts_tmp[culvert_sections] .= true
            value_l,attrib_l = rivermanagement_CalcValue(rivernet,barrier_nat,barriers_art_tmp,toosteep,culverts_tmp,
                                                         thresh_length,col_reach_length,col_reach_order,val_morph_missing1,val_imp)
            # calculate value gain
            culverts_tmp                   .= false
            culverts_tmp[culvert_ind]      .= true
            culverts_tmp[culvert_sections] .= false
            value_g,attrib_g = rivermanagement_CalcValue(rivernet,barrier_nat,barrier_art,toosteep,culverts_tmp,
                                                         thresh_length,col_reach_length,col_reach_order,val_morph_missing1,val_imp)
            for j in 1:length(culvert_sections)
                # calculate costs
                area,culvertlength,costs = rivermanagement_GetRehabCosts(rivernet,Int[],[culvert_sections[j]],
                                                                         col_node_height,col_reach_width,col_reach_length,col_reach_order)
                # write row into containers
                valueloss[n_barriers_art+findfirst(isequal(culvert_sections[j]),culvert_ind),1:6] = 
                         ["culvert",culvert_sections[j],join(culvert_sections,"|"),rivernet["attrib_reach"][culvert_sections[j],"length"],value_noart-value_l,costs]
                valuegain[n_barriers_art+findfirst(isequal(culvert_sections[j]),culvert_ind),1:6] = 
                         ["culvert",culvert_sections[j],join(culvert_sections,"|"),rivernet["attrib_reach"][culvert_sections[j],"length"],value_g-value_allart,costs]

            end
        end
    end

    # sort according to decreasing value loss and value gain
    sort!(valueloss,:ValueLoss,rev=true)
    sort!(valuegain,:ValueGain,rev=true)

    # calculate and save incremental alternatives and calculate their values and costs
    for j in 1:length(crit_label); barriers_art_tmp[j] .= barrier_art[j]; end
    culverts_tmp .= culvert
    opt_barriers = DataFrame(barrier=Int[],x=Float64[],y=Float64[])
    opt_culverts = DataFrame(culvert=Int[],id=Int[],x_start=Float64[],x_end=Float64[],y_start=Float64[],y_end=Float64[])
    for i in 1:(n_barriers_art+n_culverts)

        index = valueloss[i,"Index"]

        if valueloss[i,"Type"] == "barrier"
            for j in 1:length(crit_label); barriers_art_tmp[j][index] = false; end
            push!(opt_barriers,[index,
                                rivernet["attrib_node"][index,"x"],
                                rivernet["attrib_node"][index,"y"]])
        else 
            culverts_tmp[index] = false
            push!(opt_culverts,[index,
                                rivernet["attrib_reach"][index,"Reach_ID"],
                                rivernet["attrib_reach"][index,"x_start"],
                                rivernet["attrib_reach"][index,"x_end"],
                                rivernet["attrib_reach"][index,"y_start"],
                                rivernet["attrib_reach"][index,"y_end"]])
        end   
        CSV.write(string(dir_alternatives,"/",river,"_",alt_ext_loss,i,"_barriers.csv"),opt_barriers,delim=";")
        CSV.write(string(dir_alternatives,"/",river,"_",alt_ext_loss,i,"_culverts.csv"),opt_culverts,delim=";")
    
        value,attrib = rivermanagement_CalcValue(rivernet,barrier_nat,barriers_art_tmp,toosteep,culverts_tmp,
                                                 thresh_length,col_reach_length,col_reach_order,val_morph_missing1,val_imp)
        valueloss[i,"Value"]    = value
        valueloss[i,9:12]       = [attrib[1,1],attrib[1,2],attrib[2,1],attrib[2,2]] 
        valueloss[i,"SumCosts"] = sum(valueloss[1:i,"Costs"])
    end

    for j in 1:length(crit_label); barriers_art_tmp[j] .= barrier_art[j]; end
    culverts_tmp .= culvert
    opt_barriers = DataFrame(barrier=Int[],x=Float64[],y=Float64[])
    opt_culverts = DataFrame(culvert=Int[],id=Int[],x_start=Float64[],x_end=Float64[],y_start=Float64[],y_end=Float64[])
    for i in 1:(n_barriers_art+n_culverts)

        index = valuegain[i,"Index"]

        if valuegain[i,"Type"] == "barrier"
            for j in 1:length(crit_label); barriers_art_tmp[j][index] = false; end
            push!(opt_barriers,[index,
                                rivernet["attrib_node"][index,"x"],
                                rivernet["attrib_node"][index,"y"]])
        else 
            culverts_tmp[index] = false
            push!(opt_culverts,[index,
                                rivernet["attrib_reach"][index,"Reach_ID"],
                                rivernet["attrib_reach"][index,"x_start"],
                                rivernet["attrib_reach"][index,"x_end"],
                                rivernet["attrib_reach"][index,"y_start"],
                                rivernet["attrib_reach"][index,"y_end"]])
        end   
        CSV.write(string(dir_alternatives,"/",river,"_",alt_ext_gain,i,"_barriers.csv"),opt_barriers,delim=";")
        CSV.write(string(dir_alternatives,"/",river,"_",alt_ext_gain,i,"_culverts.csv"),opt_culverts,delim=";")
    
        value,attrib = rivermanagement_CalcValue(rivernet,barrier_nat,barriers_art_tmp,toosteep,culverts_tmp,
                                                 thresh_length,col_reach_length,col_reach_order,val_morph_missing1,val_imp)
        valuegain[i,"Value"]    = value
        valuegain[i,9:12]       = [attrib[1,1],attrib[1,2],attrib[2,1],attrib[2,2]] 
        valuegain[i,"SumCosts"] = sum(valuegain[1:i,"Costs"])
    end

    # add first row with current state
    pushfirst!(valueloss,["",0,"",0.0,0.0,0.0,0.0,value_allart,attrib_allart[1,1],attrib_allart[1,2],attrib_allart[2,1],attrib_allart[2,2]])
    pushfirst!(valuegain,["",0,"",0.0,0.0,0.0,0.0,value_allart,attrib_allart[1,1],attrib_allart[1,2],attrib_allart[2,1],attrib_allart[2,2]])

    # save table of incremental alternatives
    CSV.write(string(dir_output,"/",river,"_IncrementalAlternatives_Loss.dat"),valueloss,delim="\t")
    CSV.write(string(dir_output,"/",river,"_IncrementalAlternatives_Gain.dat"),valuegain,delim="\t")


    # optimize most promising alternatives:
    # =====================================

    better_alt  = []
    valdiff     = Float64[]
    n_obstacles = 0
    if n_opt > 1

        # find groups of barriers with close value loss:
        groups = []
        j = 1
        for i in 1:n_opt
            push!(groups,[j])
            value_l = valueloss[j+1,"ValueLoss"]    # consider that row 1 contains the current state
            if j+2 > size(valueloss)[1]; break; end 
            while valueloss[j+2,"ValueLoss"] > value_l - delta_value
                j = j + 1
                push!(groups[i],j)
                if j+2 > size(valueloss)[1]; break; end
            end
            j = j + 1
            if j+1 > size(valueloss)[1] break; end
        end

        # get permutations that keep the grous together:
        comb =  collect(Combinatorics.permutations(1:length(groups)))
        n_permutations = length(comb)
        permutations = Array{Array{Int}}(undef,n_permutations)
        for i in 1:n_permutations; permutations[i] = collect(Iterators.flatten(groups[comb[i]])); end
        n_obstacles = length(permutations[1])

        values_original = valueloss[2:(n_obstacles+1),:]
        sum_original = sum(values_original[!,"Value"])

        for i in 1:n_permutations
            # get test ranking
            test_ranking = values_original[permutations[i],:]

            # calculate values
            for j in 1:length(crit_label); barriers_art_tmp[j] .= barrier_art[j]; end
            culverts_tmp .= culvert
            for k in 1:n_obstacles
                index = test_ranking[k,"Index"]
                if test_ranking[k,"Type"] == "barrier"
                    for l in 1:length(crit_label); barriers_art_tmp[l][index] = false; end
                else 
                    culverts_tmp[index] = false
                end
                value,attrib = rivermanagement_CalcValue(rivernet,barrier_nat,barriers_art_tmp,toosteep,culverts_tmp,
                                                         thresh_length,col_reach_length,col_reach_order,val_morph_missing1,val_imp)
                test_ranking[k,"Value"]    = value
                test_ranking[k,9:12]       = [attrib[1,1],attrib[1,2],attrib[2,1],attrib[2,2]] 
                test_ranking[k,"SumCosts"] = sum(test_ranking[1:k,"Costs"])
            end

            # collect better alternatives (in an integrated sense)
            if sum(test_ranking[!,"Value"]) > sum_original
                pushfirst!(test_ranking,["",0,"",0.0,0.0,0.0,0.0,value_allart,attrib_allart[1,1],attrib_allart[1,2],attrib_allart[2,1],attrib_allart[2,2]])
                push!(better_alt,test_ranking)
                push!(valdiff,sum(test_ranking[!,"Value"])-sum_original)
            end
        end
    
    end
    println("number of better alternatives found: ",length(better_alt))
    if length(better_alt) > 0
        perm       = sortperm(valdiff,rev=true)
        valdiff    = valdiff[perm]
        better_alt = better_alt[perm]
        for i in 1:length(better_alt)
            CSV.write(string(dir_output,"/",river,"_IncrementalAlternatives_Loss_Opt",i,".dat"),better_alt[i],delim="\t")
        end
    else
        n_obstacles = min(50,size(valueloss)[1]-1)
    end


    # plot value and costs of incremental alternatives
    # ================================================

    p1 = Plots.plot([],
                    [];
                    label         = "",
                    yaxis         = "Value",
                    xaxis         = "Number of Removed Barriers",
                    xlim          = [0.0,n_barriers_art+n_culverts],
                    ylim          = [0.0,1.0],
                    title         = "Barrier removal value and costs",
                    titlefontsize = 10)
    Plots.plot!(p1,
                0:(n_barriers_art+n_culverts),
                valueloss[!,"Value"];
                linecolor = "green",
                linewidth = 1,
                label     = "Value, worst placement",
                legend    = :topleft)
    Plots.plot!(p1,
                0:(n_barriers_art+n_culverts),
                valuegain[!,"Value"];
                linecolor = "green",
                linewidth = 1,
                linestyle = :dash,
                label     = "Value, best removal")
    if river in string.(keys(Alternatives))
        for index in Alternatives[Symbol(river)]
            Plots.vline!(p1,Alternatives[Symbol(river)],label="",color="black")
        end
    end
    p1_twin = Plots.twinx(p1)
    Plots.plot!(p1_twin,
                yaxis = "Costs [MCHF]",
                xlim  = [0.0,n_barriers_art+n_culverts],
                ylim  = [0.0,valueloss[end,"SumCosts"]])
    Plots.plot!(p1_twin,
                0:(n_barriers_art+n_culverts),
                valueloss[!,"SumCosts"];
                linecolor = "blue",
                linewidth = 1,
                label     = "Costs, worst placement",
                legend    = :bottomright)
    Plots.plot!(p1_twin,
                0:(n_barriers_art+n_culverts),
                valuegain[!,"SumCosts"];
                linecolor = "blue",
                linewidth = 1,
                linestyle = :dash,
                label     = "Costs, best removal",
                legend    = :bottomright)
    Plots.savefig(p1,string(dir_output,"/",river,"_ValueCosts1a.pdf"))

    p2 = Plots.plot([],
                    [];
                    label         = "",
                    yaxis         = "Value",
                    xaxis         = "Number of Removed Barriers",
                    xlim          = [0.0,n_obstacles],
                    ylim          = [0.0,1.0],
                    title         = "Barrier removal value and costs",
                    titlefontsize = 10)
    if length(better_alt) > 0
        for i in 1:length(better_alt)
            Plots.plot!(p2,
                        0:n_obstacles,
                        better_alt[i][!,"Value"];
                        linecolor = "red",
                        linewidth = 1,
                        linestyle = if i == 1; :dash; else :dot; end,
                        label     = "")
        end
    end
    Plots.plot!(p2,
                0:(n_barriers_art+n_culverts),
                valueloss[!,"Value"];
                linecolor = "green",
                linewidth = 1,
                label     = "Value, worst placement",
                legend    = :topleft)
    Plots.plot!(p2,
                0:(n_barriers_art+n_culverts),
                valuegain[!,"Value"];
                linecolor = "green",
                linewidth = 1,
                linestyle = :dash,
                label     = "Value, best removal")
    if river in string.(keys(Alternatives))
        for index in Alternatives[Symbol(river)]
            Plots.vline!(p2,Alternatives[Symbol(river)],label="",color="black")
        end
    end
    p2_twin = Plots.twinx(p2)
    Plots.plot!(p2_twin,
                yaxis = "Costs [MCHF]",
                xlim  = [0.0,n_barriers_art+n_culverts],
                ylim  = [0.0,valueloss[end,"SumCosts"]])
   if length(better_alt) > 0
         for i in 1:length(better_alt)
            Plots.plot!(p2_twin,
                        0:n_obstacles,
                        better_alt[i][!,"SumCosts"];
                        linecolor = "red",
                        linewidth = 1,
                        linestyle = if i == 1; :dash; else :dot; end,
                        label     = "")
        end
    end
    Plots.plot!(p2_twin,
                0:(n_barriers_art+n_culverts),
                valueloss[!,"SumCosts"];
                linecolor = "blue",
                linewidth = 1,
                label     = "Costs, worst placement",
                legend    = :bottomright)
    Plots.plot!(p2_twin,
                0:(n_barriers_art+n_culverts),
                valuegain[!,"SumCosts"];
                linecolor = "blue",
                linewidth = 1,
                linestyle = :dash,
                label     = "Costs, best removal",
                legend    = :bottomright)
    Plots.savefig(p2,string(dir_output,"/",river,"_ValueCosts1b.pdf"))

    p3 = Plots.plot([],
                    [];
                    label         = "",
                    yaxis         = "Value",
                    xaxis         = "Costs [MCHF]",
                    xlim          = [0.0,valueloss[end,"SumCosts"]],
                    ylim          = [0.0,1.0],
                    title         = "Barrier removal value versus costs",
                    titlefontsize = 10)
    Plots.plot!(valueloss[!,"SumCosts"],
                valueloss[!,"Value"];
                linecolor = "green",
                linewidth = 1,
                label     = "worst placement",
                legend    = :bottomright)
    Plots.plot!(valuegain[!,"SumCosts"],
                valuegain[!,"Value"];
                linecolor = "green",
                linewidth = 1,
                linestyle = :dash,
                label     = "best removal",
                legend    = :bottomright)
    if river in string.(keys(Alternatives))
        for index in Alternatives[Symbol(river)]
            Plots.vline!(p3,valueloss[Alternatives[Symbol(river)],"SumCosts"],label="",color="black")
        end
    end
    Plots.savefig(p3,string(dir_output,"/",river,"_ValueCosts2a.pdf"))

    p4 = Plots.plot([],
                    [];
                    label         = "",
                    yaxis         = "Value",
                    xaxis         = "Costs [MCHF]",
                    xlim          = [0.0,valueloss[n_obstacles+1,"SumCosts"]],
                    ylim          = [0.0,1.0],
                    title         = "Barrier removal value versus costs",
                    titlefontsize = 10)
    if length(better_alt) > 0
        for i in 1:length(better_alt)
            Plots.plot!(better_alt[i][!,"SumCosts"],
                        better_alt[i][!,"Value"];
                        linecolor = "red",
                        linewidth = 1,
                        linestyle = if i == 1; :dash; else :dot; end,
                        label     = "")
        end
    end
    Plots.plot!(valueloss[!,"SumCosts"],
                valueloss[!,"Value"];
                linecolor = "green",
                linewidth = 1,
                label     = "worst placement",
                legend    = :bottomright)
    Plots.plot!(valuegain[!,"SumCosts"],
                valuegain[!,"Value"];
                linecolor = "green",
                linewidth = 1,
                linestyle = :dash,
                label     = "best removal",
                legend    = :bottomright)
    if river in string.(keys(Alternatives))
        for index in Alternatives[Symbol(river)]
            Plots.vline!(p2,valueloss[Alternatives[Symbol(river)],"SumCosts"],label="",color="black")
        end
    end
    Plots.savefig(p4,string(dir_output,"/",river,"_ValueCosts2b.pdf"))

end

