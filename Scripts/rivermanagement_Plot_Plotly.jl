## -------------------------------------------------------
##
## File: rivermanagement_Plot_Plotly.jl
##
## May 1, 2023 -- Peter Reichert
## peter.reichert@emeriti.eawag.ch
##
## -------------------------------------------------------

import  WebIO
import  Blink

import  PlotlyJS   # udate: ] up PlotlyJS; build PlotlyJS; backspace
                   # currently needed on some machines for interactive plots: 
                   # Pkg.pin(name="WebIO",version="0.8.17")

function rivermanagement_Plot(net;
                              title     = "",
                              col_reach = "black",  # variables or vectors of length 0, 1 or num. of reaches
                              lwd_reach = 1,
                              col_node  = "black",  # variables or vectors of length 0, 1 or num. of nodes
                              size_node = 0,
                              size_x    = 1000,
                              dodisplay = true,
                              filename  = nothing)

    # check input:
    # ============

    if ! ( haskey(net,"attrib_reach") & haskey(net,"attrib_node") )
        println("rivermanagement_Plot: *** argument \"net\" needs keys \"attrib_reach\" and \"attrib_node\" ***")
        return
    end

    # initialize variables:
    # =====================

    n_reach = nrow(net["attrib_reach"])
    n_node  = nrow(net["attrib_node"])
    
    # define layout and array for traces:
    # ===================================

    x_lim = net["x_lim"]
    y_lim = net["y_lim"] 
    if x_lim[2] == x_lim[1]
        if y_lim[2] == y_lim[1]
            x_lim = [x_lim[1]-1.0,x_lim[2]+1.0] 
            y_lim = [y_lim[1]-1.0,y_lim[2]+1.0] 
        else
            x_lim = [x_lim[1]-0.1*(y_lim[2]-y_lim[1]),x_lim[2]+0.1*(y_lim[2]-y_lim[1])]
        end
    elseif y_lim[2] == y_lim[1]
        y_lim = [y_lim[1]-0.1*(x_lim[2]-x_lim[1]),y_lim[2]+0.1*(x_lim[2]-x_lim[1])]
    end
    layout = PlotlyJS.Layout(title      = title,
                             showlegend = false,
                             autosize   = false,
                             width      = size_x,
                             height     = round(Int,size_x*(y_lim[2]-y_lim[1])/(x_lim[2]-x_lim[1])))

    traces = []

    # define reach traces:
    # ====================

    for i in 1:n_reach
        col = "black"
        if ! isa(col_reach,Array)
            col = col_reach
        elseif length(col_reach) == 1
            col = col_reach[1]
        elseif length(col_reach) == n_reach
            col = col_reach[i]
        elseif length(col_reach) != 0
            println(string("rivermanagement_Plot: *** \"col_reach\" needs to be a scalar ",
            "or a vector of length 0, 1 or number of reaches ***"))
        end
        lwd = 1
        if ! isa(lwd_reach,Array)
            lwd = lwd_reach
        elseif length(lwd_reach) == 1
            lwd = lwd_reach[1]
        elseif length(lwd_reach) == n_reach
            lwd = lwd_reach[i]
        elseif length(lwd_reach) != 0
            println(string("rivermanagement_Plot: *** \"lwd_reach\" needs to be a scalar ",
            "or a vector of length 0, 1 or number of reaches ***"))
        end

        trace = PlotlyJS.scatter(x          = net["attrib_reach"][i,"coord"].x,
                                 y          = net["attrib_reach"][i,"coord"].y,
                                 mode       = "lines",
                                 line       = PlotlyJS.attr(color=col,width=lwd),
                                 showlegend = false)

        if i == 1
            traces = [trace]
        else
            traces = vcat(traces,trace)
        end

    end

    # define node trace:
    # ==================

    col = fill("black",n_node)
    if ! isa(col_node,Array)
        col = fill(col_node,n_node)
    elseif length(col_node) == 1
        col = fill(col_node[1],n_node)
    elseif length(col_node) == n_node
        col = col_node
    elseif size(col_node) != 0
        println(string("rivermanagement_Plot: *** \"col_node\" needs to be a scalar ",
                       "or a vector of length 0, 1 or number of nodes ***"))
    end
    size = fill(2,n_node)
    if ! isa(size_node,Array)
        size = fill(size_node,n_node)
    elseif length(size_node) == 1
        size = fill(size_node[1],n_node)
    elseif length(size_node) == n_node
        size = size_node
    elseif length(size_node) != 0
        println(string("rivermanagement_Plot: *** \"size_node\" needs to be a scalar ",
                       "or a vector of length 0, 1 or number of nodes ***"))
    end

    trace = PlotlyJS.scatter(x      = net["attrib_node"][!,"x"],
                             y      = net["attrib_node"][!,"y"],
                             mode   = "markers",
                             marker = PlotlyJS.attr(color  = col,
                                                    symbol = "circle",
                                                    line=PlotlyJS.attr(width=0), # no border
                                                    size   = size),
                             showlegend = false)
    traces = vcat(traces,trace)

    # plot river network:
    # ===================

    p = PlotlyJS.plot(traces,layout)

    # display, save and return plot:
    # ==============================

    if dodisplay
        w = Blink.Window()
        Blink.body!(w,WebIO.dom"div"(p))
    end
    
    if typeof(filename) == String
        PlotlyJS.savefig(p,
                         filename,
                         width  = size_x,
                         height = round(Int,size_x*(net["y_lim"][2]-net["y_lim"][1])/(net["x_lim"][2]-net["x_lim"][1])))
    end

    return p

end
