import os
import pymupdf

rivers = ["Zulg","Gluetschbach","Rotache","Chise","Giesse","Guerbe","Kander","Birs"]
# rivers = ["Birs"]

for river in rivers:

    cur_nons = pymupdf.open(river + "_Plot_ConnectedRegions_cur_nons_Plots.pdf")
    cur_salm = pymupdf.open(river + "_Plot_ConnectedRegions_cur_salm_Plots.pdf")

    r1 = cur_nons[0].bound()
    if r1.width >= 1.25*r1.height:
        r2     = r1 + (0,r1.height,0,r1.height)
        width  = r1.width
        height = 2*r1.height
    else:
        r2 = r1 + (r1.width,0,r1.width,0)
        width  = 2*r1.width
        height = r1.height

    doc = pymupdf.open()

    page = doc.new_page(-1,width=width,height=height)
    page.show_pdf_page(r1,cur_nons)
    page.show_pdf_page(r2,cur_salm)

    cur_nons.close()
    cur_salm.close()

    i = 1
    while os.path.exists(river + "_Plot_ConnectedRegions_Opt" + str(i) + "_nons_Plots.pdf"):

        opt_nons = pymupdf.open(river + "_Plot_ConnectedRegions_Opt" + str(i) + "_nons_Plots.pdf")
        opt_salm = pymupdf.open(river + "_Plot_ConnectedRegions_Opt" + str(i) + "_salm_Plots.pdf")

        page = doc.new_page(-1,width=width,height=height)
        page.show_pdf_page(r1,opt_nons)
        page.show_pdf_page(r2,opt_salm)

        opt_nons.close()
        opt_salm.close()

        i = i+1

    nat_nons = pymupdf.open(river + "_Plot_ConnectedRegions_nat_nons_Plots.pdf")
    nat_salm = pymupdf.open(river + "_Plot_ConnectedRegions_nat_salm_Plots.pdf")

    page = doc.new_page(-1,width=width,height=height)
    page.show_pdf_page(r1,nat_nons)
    page.show_pdf_page(r2,nat_salm)

    nat_nons.close()
    nat_salm.close()
    
    doc.save(river + "_Plot_ConnectedRegions_Opts_Plots.pdf",garbage=3,deflate=True)

