import Pkg
Pkg.activate(".")
Pkg.instantiate()

Pkg.add("Statistics")
Pkg.add("DataFrames")
Pkg.add("CSV")

Pkg.add("Plots")

Pkg.add("WebIO")
# Pkg.pin(name="WebIO",version="0.8.21")
Pkg.add("Blink")
Pkg.add("PlotlyJS")
Pkg.build("PlotlyJS")

Pkg.resolve()



