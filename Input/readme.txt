Preprocessing of input files
============================

- Replace field codes for missing like "NA", "<Null>", 0, etc. by empty fields

- Add a column "BarrierType" (G is colmn "Type_Name":
  =IF(OR(G2="Drop natural";G2="Absturz_natuerlich");1;IF(G2="Fischpass";0;2))
  
  
  