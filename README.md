# rivermanagement

Julia scripts for calculating catchment-based ecological state assessment for river networks, currently focusing on fish migration.

Description of the underlying concept:

Reichert, P., Langhans, S., Lienert, J. and Schuwirth, N.\
The Conceptual Foundation of Environmental Decision Support.\
Journal of Environmental Management 154, 316-332, 2015.\
https://doi.org/10.1016/j.jenvman.2015.01.053

Kuemmerlen, M., Reichert, P., Siber, R. and Schuwirth, N.\
Ecological assessment of river networks: From reach to catchment scale.\
Science of the Total Environment 650, 1613-1627, 2019.\
https://doi.org/10.1016/j.scitotenv.2018.09.019

Publication of the results (in German):

Reichert, P., Ambord, I., von Wattenwyl, K., Schläppi, S., Pompini, M., Dönni, W., Thomas, G., Siber, R. und Schuwirth, N.\
Verfahren zur Priorisierung der Beseitigung künstlicher Fischwanderhindernisse in Fliessgewässersystemen.\
Bundesamt für Umwelt (BAFU), 2023.\
https://peterreichert.github.io/doc/UV-1208-D_Fliessgewaesser-2023_Anhang.pdf

Reichert, P., Ambord, I., von Wattenwyl, K., Schläppi, S., Pompini, M., Dönni, W., Thomas, G., Siber, R. und Schuwirth, N.\
Priorisierung der Sanierung künstlicher Fischwanderhindernisse.
Aqua & Gas 3, 68 - 72, 2024.\
https://peterreichert.github.io/doc/Reichert_2024_Fischwanderhindernisse.pdf

## Executable Scripts

### rivermanagement_Interactive.jl

Read a river network, calculate attributes, and produce an interactive plot that allows the user to select artificial barriers to remove and produce a new plot for the rehabilitated river network.

### rivermanagement_Batch.jl

Read river networks, calculate attributes, and produce standard river network plots.

### rivermanagement_Optimize.jl

Read river networks and develop incremental obstacle removal strategies.
The steps of the strategies can subsequently be visualized with the script ``rivermanagement_Batch.jl''

